<?php

/**
 * Обновление данных о городах, странах и станциях.
 */

header('Content-Type: text/html; charset=utf-8');

require_once( explode("wp-content", __FILE__)[0] . "wp-load.php" );

if (isset($_GET['to'])) {
    $to = $_GET['to'];
}

if ($to != get_option('to_secret')) {

    header("HTTP/1.0 404 Not Found");
    exit;

} else {

    $text = '';
    $authdata = base64_encode(get_option('to_username') . ":" . get_option('to_password'));
    $host_api = get_option('to_host_api');
    $param_pos = get_option('to_param_pos');

    if (to_country($authdata, $host_api, $param_pos) == '') {
        $text .= '<p style="color: green;">Данные о странах успешно обновлены!</p>';
    } else {
        $text .= '<p style="color: red;">Не удалось получить данные о странах!</p>';
    }

    if (to_city($authdata, $host_api, $param_pos) == '') {
        $text .= '<p style="color: green;">Данные о городах успешно обновлены!</p>';
    } else {
        $text .= '<p style="color: red;">Не удалось получить данные о городах!</p>';
    }

    if (to_nodes($authdata, $host_api, $param_pos) == '') {
        $text .= '<p style="color: green;">Данные о станциях успешно обновлены!</p>';
    } else {
        $text .= '<p style="color: red;">Не удалось получить данные о станциях!</p>';
    }

    echo $text;
}

?>
