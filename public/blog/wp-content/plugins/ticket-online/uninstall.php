<?php

/**
 * This file is part of is free software.
 */
/**
 * Created on: 09.04.2019, 12:36:31
 * Author: Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

if (!defined('WP_UNINSTALL_PLUGIN'))
    exit;

// Проверка пройдена успешно. Начиная отсюда удаляем опции и всё остальное.
delete_option('to_username');
delete_option('to_password');
delete_option('to_host_api');
delete_option('to_param_pos');
delete_option('to_secret');

global $wpdb;

$table_name = $wpdb->get_blog_prefix() . 'to_country';
$wpdb->query("DROP TABLE " . $table_name);

$table_name = $wpdb->get_blog_prefix() . 'to_city';
$wpdb->query("DROP TABLE " . $table_name);

$table_name = $wpdb->get_blog_prefix() . 'to_nodes';
$wpdb->query("DROP TABLE " . $table_name);
