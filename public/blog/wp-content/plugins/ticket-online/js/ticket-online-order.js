/**
 * This file is part of is free software.
 */

jQuery(document).ready(function () {

    // Страница оформления билета ввод данных пасажиров

    jQuery.datetimepicker.setLocale('ru');

    var baby_year = (new Date()).getFullYear() - 5;

    jQuery('#birdthday1').datetimepicker({
        lang: 'ru',
        startDate: '1950/01/01', // or 1986/12/08
        yearStart: '1900',
        yearEnd: (new Date()).getFullYear(),
        minDate: '1900/01/01', // yesterday is minimum date (for today use 0 or -1970/01/01)
        maxDate: '+1970/01/01', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 1,
    });

    jQuery('#birdthday2').datetimepicker({
        lang: 'ru',
        startDate: '1950/01/01', // or 1986/12/08
        yearStart: '1900',
        yearEnd: (new Date()).getFullYear(),
        minDate: '1900/01/01', // yesterday is minimum date (for today use 0 or -1970/01/01)
        maxDate: '+1970/01/01', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 1,
    });

    jQuery('#birdthday3').datetimepicker({
        lang: 'ru',
        startDate: '1950/01/01', // or 1986/12/08
        yearStart: '1900',
        yearEnd: (new Date()).getFullYear(),
        minDate: '1900/01/01', // yesterday is minimum date (for today use 0 or -1970/01/01)
        maxDate: '+1970/01/01', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 1,
    });

    jQuery('#birdthday4').datetimepicker({
        lang: 'ru',
        startDate: '1950/01/01', // or 1986/12/08
        yearStart: '1900',
        yearEnd: (new Date()).getFullYear(),
        minDate: '1900/01/01', // yesterday is minimum date (for today use 0 or -1970/01/01)
        maxDate: '+1970/01/01', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 1,
    });

    jQuery('#birdthday5').datetimepicker({
        lang: 'ru',
        yearStart: baby_year,
        yearEnd: (new Date()).getFullYear(),
        minDate: '1900/01/01', // yesterday is minimum date (for today use 0 or -1970/01/01)
        maxDate: '+1970/01/01', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 1,
    });

    jQuery('#birdthday6').datetimepicker({
        lang: 'ru',
        yearStart: baby_year,
        yearEnd: (new Date()).getFullYear(),
        minDate: '1900/01/01', // yesterday is minimum date (for today use 0 or -1970/01/01)
        maxDate: '+1970/01/01', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 1,
    });

    jQuery('.add_child_btn').on('click', function (e) {
        e.preventDefault();
        var a = jQuery('.children_container .disbled').length;
        if (a <= 1) {
            jQuery('.child_ticket').removeClass('disbled').addClass('enebled');
            jQuery(this).addClass('disabled').removeClass('enebled');
            req();
        } else if (a < 2) {
            jQuery('.child_ticket').removeClass('disbled').addClass('enebled');
            req();
        } else {
            jQuery('.child_ticket').first().removeClass('disbled').addClass('enebled');
            req();
        }
    });

    jQuery('.close_ticket_section1').on('click', function (e) {
        e.preventDefault();
        jQuery(this).parent().addClass('disbled');
        jQuery(this).parent().removeClass('enebled');

        jQuery('#add_baby_btn').removeClass('disaled_btn');
        jQuery(this).parent().find('.reqired');
        var chReq = jQuery(document).find('.reqired');
        chReq.each(function () {
            this.setAttribute('data-validation', '');
        });
        var a = jQuery('.children_container .disbled').length;
        if (a <= 2) {
            jQuery('.add_child_btn').removeClass('disabled').addClass('enebled');
        }

        jQuery("#name5").val('');
        jQuery("#lastname5").val('');
        jQuery("#fathername5").val('');
        jQuery("#select_sex5").val('');
        jQuery("#birdthday5").val('');
        jQuery("#document_type5").val('');
        jQuery("#docnumber5").val('');
        jQuery("#select_country5").val('');
    });

    jQuery('.close_ticket_section2').on('click', function (e) {
        e.preventDefault();
        jQuery(this).parent().addClass('disbled');
        jQuery(this).parent().removeClass('enebled');
        jQuery('#add_baby_btn').removeClass('disaled_btn');
        jQuery(this).parent().find('.reqired');
        var chReq = jQuery(document).find('.reqired');
        chReq.each(function () {
            this.setAttribute('data-validation', '');
        });
        var a = jQuery('.children_container .disbled').length;
        if (a <= 2) {
            jQuery('.add_child_btn').removeClass('disabled').addClass('enebled');
        }

        jQuery("#name6").val('');
        jQuery("#lastname6").val('');
        jQuery("#fathername6").val('');
        jQuery("#select_sex6").val('');
        jQuery("#birdthday6").val('');
        jQuery("#document_type6").val('');
        jQuery("#docnumber6").val('');
        jQuery("#select_country6").val('');
    });

    function req() {
        var checkReq = jQuery(document).find('.ticket_section.enebled .reqired');
        checkReq.each(function () {
            this.setAttribute('data-validation', 'required');
        })
    }

    req();

    // Скрипт валидации полей взял отсюда. Там есть полная документация по его настройке http://www.formvalidator.net/#configuration_html5
    jQuery.validate({
        form: '#registration_tickets'
    });

    jQuery(document).on('click', '.more_info', function () {
        jQuery(this).addClass('invisible');
        jQuery(this).parent().find('.hidden_details').addClass('opened').stop(false, true).slideDown(300);
    });

    jQuery(document).on('click', '.close_details', function () {
        jQuery(this).parent().parent().find('.more_info').removeClass('invisible');
        jQuery(this).parent().removeClass('opened').stop(false, true).slideUp(100);
    });

    jQuery(document).on('click', '.more_info2', function () {
        jQuery(this).addClass('invisible');
        jQuery(this).parent().find('.hidden_details2').addClass('opened').stop(false, true).slideDown(300);
    });

    jQuery(document).on('click', '.close_details2', function () {
        jQuery(this).parent().parent().find('.more_info2').removeClass('invisible');
        jQuery(this).parent().removeClass('opened').stop(false, true).slideUp(100);
    });

    // Отправка запроса на создание заказа
    jQuery("#create_order").live('click', function (event) {
        event.preventDefault();

        jQuery('#registration_tickets').removeClass('show_ord');
        jQuery('#registration_tickets').addClass('hidden_ord');
        jQuery('#to_result_html_order').removeClass('hidden_ord');
        jQuery('#to_result_html_order').addClass('show_ord');

        jQuery('#to_result_html_order').html('<div class="order_container">Подождите, идёт запрос резервации билетов</div>');
        jQuery('html, body').animate({
            scrollTop: jQuery('#to_result_html_order').offset().top - 150
        }, 500);

        if (jQuery(".ticket_section1").hasClass("enebled")) {
            var name1 = jQuery("#name1").val();
            var lastname1 = jQuery("#lastname1").val();
            var fathername1 = jQuery("#fathername1").val();
            var select_sex1 = jQuery("#select_sex1").val();
            var birdthday1 = jQuery("#birdthday1").val();
            var document_type1 = jQuery("#document_type1").val();
            var docnumber1 = jQuery("#docnumber1").val();
            var select_country1 = jQuery("#select_country1").val();
            var select_tariff1 = jQuery("#select_tariff1").val();

            var ticket_section1 = {
                name1: name1,
                lastname1: lastname1,
                fathername1: fathername1,
                select_sex1: select_sex1,
                birdthday1: birdthday1,
                document_type1: document_type1,
                docnumber1: docnumber1,
                select_country1: select_country1,
                select_tariff1: select_tariff1
            };

            ticket_1 = JSON.stringify(ticket_section1);
        }

        if (jQuery(".ticket_section2").hasClass("enebled")) {
            var lastname2 = jQuery("#lastname2").val();
            var fathername2 = jQuery("#fathername2").val();
            var select_sex2 = jQuery("#select_sex2").val();
            var birdthday2 = jQuery("#birdthday2").val();
            var document_type2 = jQuery("#document_type2").val();
            var docnumber2 = jQuery("#docnumber2").val();
            var select_country2 = jQuery("#select_country2").val();
            var select_tariff2 = jQuery("#select_tariff2").val();
        }

        if (jQuery(".ticket_section3").hasClass("enebled")) {
            var name3 = jQuery("#name3").val();
            var lastname3 = jQuery("#lastname3").val();
            var fathername3 = jQuery("#fathername3").val();
            var select_sex3 = jQuery("#select_sex3").val();
            var birdthday3 = jQuery("#birdthday3").val();
            var document_type3 = jQuery("#document_type3").val();
            var docnumber3 = jQuery("#docnumber3").val();
            var select_country3 = jQuery("#select_country3").val();
            var select_tariff3 = jQuery("#select_tariff3").val();
        }

        if (jQuery(".ticket_section4").hasClass("enebled")) {
            var name4 = jQuery("#name4").val();
            var lastname4 = jQuery("#lastname4").val();
            var fathername4 = jQuery("#fathername4").val();
            var select_sex4 = jQuery("#select_sex4").val();
            var birdthday4 = jQuery("#birdthday4").val();
            var document_type4 = jQuery("#document_type4").val();
            var docnumber4 = jQuery("#docnumber4").val();
            var select_country4 = jQuery("#select_country4").val();
            var select_tariff4 = jQuery("#select_tariff4").val.val();
        }

        if (jQuery(".ticket_section5").hasClass("enebled")) {
            var name5 = jQuery("#name5").val();
            var lastname5 = jQuery("#lastname5").val();
            var fathername5 = jQuery("#fathername5").val();
            var select_sex5 = jQuery("#select_sex5").val();
            var birdthday5 = jQuery("#birdthday5").val();
            var document_type5 = jQuery("#document_type5").val();
            var docnumber5 = jQuery("#docnumber5").val();
            var select_country5 = jQuery("#select_country5").val();
            var select_tariff5 = jQuery("#select_tariff5").val();
        }

        if (jQuery(".ticket_section6").hasClass("enebled")) {
            var name6 = jQuery("#name6").val();
            var lastname6 = jQuery("#lastname6").val();
            var fathername6 = jQuery("#fathername6").val();
            var select_sex6 = jQuery("#select_sex6").val();
            var birdthday6 = jQuery("#birdthday6").val();
            var document_type6 = jQuery("#document_type6").val();
            var docnumber6 = jQuery("#docnumber6").val();
            var select_country6 = jQuery("#select_country6").val();
            var select_tariff6 = jQuery("#select_tariff6").val();
        }

        jQuery.ajax({
            url: window.ajaxUrl,
            method: 'post',
            data: {
                action: 'ajax_to_create_order',
                ticket_1: ticket_1,
            },
            success: function (response) {
                jQuery('html, body').animate({
                    scrollTop: jQuery('#to_result_html_order').offset().top - 150
                }, 500);
                jQuery('#to_result_html_order').html(response);
            }
        });
    });

    jQuery("#change_order").live('click', function (event) {
        event.preventDefault();
        jQuery('#registration_tickets').addClass('show_ord');
        jQuery('#registration_tickets').removeClass('hidden_ord');
        jQuery('#to_result_html_order').addClass('hidden_ord');
        jQuery('#to_result_html_order').removeClass('show_ord');
        jQuery('html, body').animate({
            scrollTop: jQuery('#registration_tickets').offset().top - 150
        }, 500);
    });

    // Очистка полей блоков формы заказа
    jQuery(".clear_data_order1").live('click', function (event) {
        event.preventDefault();
        jQuery("#name1").val('');
        jQuery("#lastname1").val('');
        jQuery("#fathername1").val('');
        jQuery("#select_sex1").val('');
        jQuery("#birdthday1").val('');
        jQuery("#document_type1").val('');
        jQuery("#docnumber1").val('');
        jQuery("#select_country1").val('');
        jQuery("#select_tariff1").prop('selectedIndex', 0);
    });

    jQuery(".clear_data_order2").live('click', function (event) {
        event.preventDefault();
        jQuery("#name2").val('');
        jQuery("#lastname2").val('');
        jQuery("#fathername2").val('');
        jQuery("#select_sex2").val('');
        jQuery("#birdthday2").val('');
        jQuery("#document_type2").val('');
        jQuery("#docnumber2").val('');
        jQuery("#select_country2").val('');
        jQuery("#select_tariff2").prop('selectedIndex', 0);
    });

    jQuery(".clear_data_order3").live('click', function (event) {
        event.preventDefault();
        jQuery("#name3").val('');
        jQuery("#lastname3").val('');
        jQuery("#fathername3").val('');
        jQuery("#select_sex3").val('');
        jQuery("#birdthday3").val('');
        jQuery("#document_type3").val('');
        jQuery("#docnumber3").val('');
        jQuery("#select_country3").val('');
        jQuery("#select_tariff3").prop('selectedIndex', 0);
    });

    jQuery(".clear_data_order4").live('click', function (event) {
        event.preventDefault();
        jQuery("#name4").val('');
        jQuery("#lastname4").val('');
        jQuery("#fathername4").val('');
        jQuery("#select_sex4").val('');
        jQuery("#birdthday4").val('');
        jQuery("#document_type4").val('');
        jQuery("#docnumber4").val('');
        jQuery("#select_country4").val('');
        jQuery("#select_tariff4").prop('selectedIndex', 0);
    });

    jQuery(".clear_data_order5").live('click', function (event) {
        event.preventDefault();
        jQuery("#name5").val('');
        jQuery("#lastname5").val('');
        jQuery("#fathername5").val('');
        jQuery("#select_sex5").val('');
        jQuery("#birdthday5").val('');
        jQuery("#document_type5").val('');
        jQuery("#docnumber5").val('');
        jQuery("#select_country5").val('');
    });

    jQuery(".clear_data_order6").live('click', function (event) {
        event.preventDefault();
        jQuery("#name6").val('');
        jQuery("#lastname6").val('');
        jQuery("#fathername6").val('');
        jQuery("#select_sex6").val('');
        jQuery("#birdthday6").val('');
        jQuery("#document_type6").val('');
        jQuery("#docnumber6").val('');
        jQuery("#select_country6").val('');
    });

    // Отмена заказа (резервации)
    jQuery("#cancel_order").live('click', function (event) {
        event.preventDefault();

        var cancel_order_id = parseInt(jQuery(this).attr('data-cancel_order'));

        jQuery.ajax({
            url: window.ajaxUrl,
            method: 'post',
            data: {
                action: 'ajax_to_cancel_order',
                cancel_order_id: cancel_order_id
            },
            success: function (response) {
                jQuery('html, body').animate({
                    scrollTop: jQuery('#registration_tickets').offset().top - 150
                }, 500);
                jQuery('#form_order_form_1').html('Производится отмена заказа');
                jQuery('#form_order_form_1').html(response);

                if (window.history.replaceState) {
                    // для невозможности возврата на форму при отмене заказа, убираем гет из ссылки и сбрасываем историю браузера (кнопка назад)
                    window.history.replaceState('page', 'Title', '/order/');
                }
            }
        });
    });

    // Оплата заказа
    jQuery("#payment_order").live('click', function (event) {
        event.preventDefault();
        // Переходим к оплате
        alert('Всё ок, идём на оплату');
    });

});
