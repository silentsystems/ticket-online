/**
 * This file is part of is free software.
 */

jQuery(document).ready(function () {

    // Количество билетов

    jQuery(document).on('click', '.input-number-more', function () {
        summery()
    });
    jQuery(document).on('click', '.input-number-less', function () {
        summery()
    });

    function summery(e) {
        var adultSum = parseInt(jQuery('.adult .adult-num').val());
        var less10Sum = parseInt(jQuery('.childrensLess10 .less10').val());
        var less5Sum = parseInt(jQuery('.childrensLess5 .less5').val());
        var allSum = adultSum + less10Sum + less5Sum;
        jQuery('.all_inp_sum').val(allSum);
        if (allSum >= 4) {
            disInp();
        } else {
            anInp()
        }
        if (adultSum === 1 && allSum < 5) {
            jQuery('.childrensLess5 .less5').attr('max', '1');
        } else if (adultSum === 2 && allSum < 5) {
            jQuery('.childrensLess5 .less5').attr('max', '2');
        } else if (adultSum > 2 && allSum < 5) {
            jQuery('.childrensLess5 .less5').attr('max', '1');
        }
    }

    summery();

    function disInp() {
        jQuery('.input-number-more').css('pointerEvents', 'none');
    }
    function anInp() {
        jQuery('.input-number-more').css('pointerEvents', 'auto');
    }

    var a = document.querySelector('to-fpc__berth-place');

    // input даты туда

    jQuery.datetimepicker.setLocale('ru');

    jQuery('#date0').datetimepicker({
        end: '#date0',
        cells: [1, 3],
        lang: 'ru',
        minDate: 0, // yesterday is minimum date(for today use 0 or -1970/01/01)
        maxDate: '+1970/02/16', // tomorrow is maximum date calendar
        timepicker: false,
        format: 'Y-m-d',
        numberOfMonths: 3,
        onChangeDateTime: date1,
    });

    // input даты обратно

    function date1() {
        var date0 = jQuery('#date0').val();
        var a = [],
            year,
            month,
            day;
        a = date0.split('-');
        year = a[0];
        month = a[1];
        day = a[2];
        // console.log('-1970/'+a[1]+'/'+a[2]+'')
        jQuery('#date1').datetimepicker({
            end: '#date1',
            cells: [1, 3],
            lang: 'ru',
            minDate: '' + year + '/' + month + '/' + day + '', // Дата выбрана в инпуте "Выбор даты туда"
            maxDate: '+1970/02/16', // tomorrow is maximum date calendar
            timepicker: false,
            format: 'Y-m-d'
        });
    }

    // input станции отправления

    jQuery('.name0_start').keydown(function (eventObject) {
        var searchTerm = jQuery(this).val();
        jQuery.ajax({
            url: window.ajaxUrl,
            type: 'POST',
            data: {
                'action': 'places_ajax_search',
                'term': searchTerm
            },
            success: function (result) {
                jQuery('.name0_start-search').fadeIn().html(result);
            }
        });
    });

    jQuery(document).click(function (event) {
        if (jQuery(event.target).closest(".name0_start-search").length)
            return;
        jQuery(".name0_start-search").fadeOut();
        event.stopPropagation();
    });

    jQuery('.name0_start-search').on('click', 'li', function (event) {
        var node_id = jQuery(this).children().data("node_id");
        var node_nameru = jQuery(this).children().data("node_nameru");
        jQuery("#name0").val(node_nameru);
        jQuery("#name0_0").val(node_id);
        jQuery(".name0_start-search").fadeOut();
        event.stopPropagation();
    });

    // input станции прибытия

    jQuery('.name1_end').keydown(function (eventObject) {
        var searchTerm = jQuery(this).val();
        jQuery.ajax({
            url: window.ajaxUrl,
            type: 'POST',
            data: {
                'action': 'places_ajax_search',
                'term': searchTerm
            },
            success: function (result) {
                jQuery('.name1_end-search').fadeIn().html(result);
            }
        });
    });

    jQuery(document).click(function (event) {
        if (jQuery(event.target).closest(".name1_end-search").length)
            return;
        jQuery(".name1_end-search").fadeOut();
        event.stopPropagation();
    });

    jQuery('.name1_end-search').on('click', 'li', function (event) {
        var node_id = jQuery(this).children().data("node_id");
        var node_nameru = jQuery(this).children().data("node_nameru");
        jQuery("#name1").val(node_nameru);
        jQuery("#name1_1").val(node_id);
        jQuery(".name1_end-search").fadeOut();
        event.stopPropagation();

    });

    // Отправка запроса форма 1 (запрос поездов)

    jQuery("#Submit1").on("click", function (event) {
        event.preventDefault();
        localStorage.removeItem("direct_data");
        jQuery('#to_result_html').html('Идет поиск подходящих маршрутов');
        var Origin = jQuery("#name0_0").val();
        var Destination = jQuery("#name1_1").val();
        var DepartureDate = jQuery("#date0").val() + 'T00:00:00';
        var TimeFrom = 0;
        var TimeTo = 24;
        jQuery.ajax({
            url: window.ajaxUrl,
            method: 'post',
            data: {
                action: 'ajax_to_trainpricing',
                Origin: Origin,
                Destination: Destination,
                DepartureDate: DepartureDate,
                TimeFrom: TimeFrom,
                direct: 1,
                TimeTo: TimeTo
            },
            success: function (response) {
                jQuery('html, body').animate({
                    scrollTop: jQuery('#to_result_html').offset().top - 150
                }, 500);
                jQuery('#to_result_html').html(response);
            }
        });
    });

    // Активируем-деактивируем инпуты даты-времени обратно

    jQuery('#DirToggle').click(function () {
        if (jQuery(this).is(':checked')) {
            jQuery('#date1').removeAttr("disabled");
            // Переписал скрипт инициализации на строке 65 <-----
        } else {
            // add disabled
            jQuery('#date1').attr('disabled', 'disabled');
        }
    });

    // Если не выбрана дата поездки туда

    jQuery('#date1').click(function () {
        var d0Val = jQuery('#date0').val();
        if (d0Val === '') {
            alert('Нужно выбрать дату поездки туда')
        }
    });

    // Запрос вагонов по конкретному поезду

    jQuery(".car_button").live('click', function (event) {

        event.preventDefault();

        jQuery(".train_cell_cars").html('');

        if (jQuery(this).hasClass('car_button_active') == true) {

            jQuery(this).removeClass("car_button_active");

        } else {

            jQuery('html, body').animate({
                scrollTop: jQuery(this).offset().top - 150
            }, 500);

            jQuery(".car_button").removeClass("car_button_active");

            jQuery(this).addClass("car_button_active");

            var train_OriginCode = jQuery(this).find(".train_OriginCode").val();
            var train_DestinationCode = jQuery(this).find(".train_DestinationCode").val();
            var train_DepartureDate = jQuery(this).find(".train_DepartureDate").val();
            var train_TrainNumber = jQuery(this).find(".train_TrainNumber").val();
            var train_CarType = jQuery(this).find(".train_CarType").val();
            var train_RoutePolicy = jQuery(this).find(".train_RoutePolicy").val();

            var train_LocalDepartureTime = jQuery(this).find(".LocalDepartureTime").val();
            var train_LocalDepartureDate = jQuery(this).find(".LocalDepartureDate").val();
            var train_OriginStation_name = jQuery(this).find(".OriginStation_name").val();

            var train_LocalArrivalTime = jQuery(this).find(".LocalArrivalTime").val();
            var train_LocalArrivalDate = jQuery(this).find(".LocalArrivalDate").val();
            var train_DestinationStation_name = jQuery(this).find(".DestinationStation_name").val();

            var min_price_car = jQuery(this).find(".min_price_car").val();
            var test_price_car = jQuery(this).find(".test_price_car").val();

            var train_TariffType = jQuery(this).find(".train_TariffType").val();
            var train_SpecialPlacesDemand = jQuery(this).find(".train_SpecialPlacesDemand").val();
            var train_GetOnlyCarTransportationCoaches = jQuery(this).find(".train_GetOnlyCarTransportationCoaches").val();
            var train_GetOnlyNonRefundableTariffs = jQuery(this).find(".train_GetOnlyNonRefundableTariffs").val();

            jQuery('#' + train_TrainNumber).html('Подождите, идёт поиск мест.');

            jQuery.ajax({
                url: window.ajaxUrl,
                method: 'post',
                data: {
                    action: 'ajax_to_carpricing',
                    train_OriginCode: train_OriginCode,
                    train_DestinationCode: train_DestinationCode,
                    train_DepartureDate: train_DepartureDate,
                    train_TrainNumber: train_TrainNumber,
                    train_CarType: train_CarType,
                    train_LocalDepartureTime: train_LocalDepartureTime,
                    train_LocalDepartureDate: train_LocalDepartureDate,
                    train_OriginStation_name: train_OriginStation_name,
                    train_LocalArrivalTime: train_LocalArrivalTime,
                    train_LocalArrivalDate: train_LocalArrivalDate,
                    train_DestinationStation_name: train_DestinationStation_name,
                    min_price_car: min_price_car,
                    test_price_car: test_price_car,
                    train_TariffType: train_TariffType,
                    train_RoutePolicy: train_RoutePolicy,
                    train_SpecialPlacesDemand: train_SpecialPlacesDemand,
                    train_GetOnlyCarTransportationCoaches: train_GetOnlyCarTransportationCoaches,
                    train_GetOnlyNonRefundableTariffs: train_GetOnlyNonRefundableTariffs
                },
                success: function (response) {
                    jQuery('#' + train_TrainNumber).html(response);
                }
            });

        }

    });

    // Вывод схемы вагона / формы выбора мест

    jQuery(".car_number").live('click', function (event) {

        event.preventDefault();

        var car_seat = '';

        jQuery(".train_cell_cars_out").html('');
        jQuery(".buton_order_div").html('');

        if (jQuery(this).hasClass('car_number_active') == true) {

            jQuery(this).removeClass("car_number_active");

        } else {

            // arr_seat = [];

            jQuery('html, body').animate({
                scrollTop: jQuery(this).offset().top - 200
            }, 500);
            jQuery(".car_number").removeClass("car_number_active");

            jQuery(this).addClass("car_number_active");

            var car_FreePlaces = jQuery(this).find(".car_FreePlaces").val();
            var car_html_id = jQuery(this).find(".car_html_id").val();
            var train_LocalDepartureTime = jQuery(this).find(".LocalDepartureTime").val();
            var train_LocalDepartureDate = jQuery(this).find(".LocalDepartureDate").val();
            var train_OriginStation_name = jQuery(this).find(".OriginStation_name").val();
            var train_LocalArrivalTime = jQuery(this).find(".LocalArrivalTime").val();
            var train_LocalArrivalDate = jQuery(this).find(".LocalArrivalDate").val();
            var train_DestinationStation_name = jQuery(this).find(".DestinationStation_name").val();
            var RoutePolicy = jQuery(this).find(".RoutePolicy").val();
            var min_price_car = jQuery(this).find(".min_price_car").val();
            var test_price_car = jQuery(this).find(".test_price_car").val();
            var car_html_id_order = jQuery(this).find(".car_html_id_order").val();
            var CarType = jQuery(this).find(".CarType").val();
            var ServiceClass = jQuery(this).find(".ServiceClass").val();
            var service_prov_input = jQuery(this).find(".service_prov_input").val();

            jQuery('#' + car_html_id).html('Подождите, идёт поиск мест.');

            // var items = [];

            jQuery.ajax({
                url: window.ajaxUrl,
                method: 'post',
                data: {
                    action: 'ajax_to_carseat',
                    car_FreePlaces: car_FreePlaces,
                    CarType: CarType,
                    train_LocalDepartureTime: train_LocalDepartureTime,
                    train_LocalDepartureDate: train_LocalDepartureDate,
                    train_OriginStation_name: train_OriginStation_name,
                    train_LocalArrivalTime: train_LocalArrivalTime,
                    train_LocalArrivalDate: train_LocalArrivalDate,
                    train_DestinationStation_name: train_DestinationStation_name,
                    RoutePolicy: RoutePolicy,
                    min_price_car: min_price_car,
                    test_price_car: test_price_car,
                    car_html_id: car_html_id_order,
                    ServiceClass: ServiceClass,
                    service_prov_input: service_prov_input,
                },
                success: function (response) {
                    jQuery('#' + car_html_id).html(response);
                }
            });

        }

    });

    // Выбор по параметрам

    var saetArr = []; // Сюда записываются первое и последнее места.

    // Выбор места по параметрам
    jQuery('.train_cell_cars_in_selects').live('change', function () {
        var Origin_name = jQuery("#name0").val();
        var Origin_id = jQuery("#name0_0").val();
        var Destination_name = jQuery("#name1").val();
        var Destination_id = jQuery("#name1_1").val();
        var train_LocalDepartureTime = jQuery("#LocalDepartureTime").val();
        var train_LocalDepartureDate = jQuery("#LocalDepartureDate").val();
        var train_OriginStation_name = jQuery("#OriginStation_name").val();
        var train_LocalArrivalTime = jQuery("#LocalArrivalTime").val();
        var train_LocalArrivalDate = jQuery("#LocalArrivalDate").val();
        var train_DestinationStation_name = jQuery("#DestinationStation_name").val();
        var RoutePolicy = jQuery("#RoutePolicy").val();
        var DepartureDateDirect = jQuery("#date0").val() + 'T00:00:00';

        if (jQuery('#DirToggle').prop('checked')) {
            var DepartureDateReturn = jQuery("#date1").val() + 'T00:00:00';
        } else {
            var DepartureDateReturn = false;
        }

        var CarType = jQuery('#CarType').val();
        var ServiceClass = jQuery('#ServiceClass').val();
        var service_prov_input = jQuery('#service_prov_input').val();
        var count_adult = parseInt(jQuery('.adult .adult-num').val());
        var count_children = parseInt(jQuery('.childrensLess10 .less10').val());
        var count_baby = parseInt(jQuery('.childrensLess5 .less5').val());
        var seats_number = count_adult + count_children + count_baby;
        const min_price_car = jQuery("#min_price_car").val();
        const test_price_car = jQuery("#test_price_car").val();
        const seats_number_no_baby = count_adult + count_children;
        const all_price_car = seats_number_no_baby * min_price_car;
        const all_test_price_car = seats_number_no_baby * test_price_car;
        var car_html_id = jQuery(this).find('#select_car_html_id').val();
        var sel1 = 0,
            sel2 = 0;
        var sel3 = 0;
        var sel4 = 0;
        var sel5 = 0;

        sel1 = jQuery(this).find('#select1').val();
        sel2 = jQuery(this).find('#select2').val();
        saetArr = [sel1, sel2];
        sel3 = jQuery(this).find('#select3').val();
        sel4 = jQuery(this).find('#select4').val();
        sel5 = jQuery(this).find('#select5').val();

        if (saetArr.length === 2 || seats_number === 1) {
            //  Выводит массив с 2-мя значениями первое выбранное и второе выбранное место.
            var jsonString = JSON.stringify(saetArr);

            // Это условие если отмечен чекбокс обратно, но мы ещё не выбирали обратны билеты (то есть это условие ещё не срабатывало)
            if (jQuery('#DirToggle').prop('checked') && localStorage.getItem('direct_data') == undefined) {
                jQuery.ajax({
                    url: window.ajaxUrl,
                    method: 'post',
                    data: {
                        action: 'ajax_to_seat',
                        only_direct: 0,
                        count_adult: count_adult,
                        count_children: count_children,
                        count_baby: count_baby,
                        button_id: car_html_id,
                        Origin_name: Origin_name,
                        Origin_id: Origin_id,
                        Destination_name: Destination_name,
                        Destination_id: Destination_id,
                        DepartureDateDirect: DepartureDateDirect,
                        DepartureDateReturn: DepartureDateReturn,
                        service_prov_input: service_prov_input,
                        CarType: CarType,
                        train_LocalDepartureTime: train_LocalDepartureTime,
                        train_LocalDepartureDate: train_LocalDepartureDate,
                        train_OriginStation_name: train_OriginStation_name,
                        train_LocalArrivalTime: train_LocalArrivalTime,
                        train_LocalArrivalDate: train_LocalArrivalDate,
                        train_DestinationStation_name: train_DestinationStation_name,
                        ServiceClass: ServiceClass,
                        Type_coupe: sel3,
                        Jarus: sel4,
                        Type_seat: sel5,
                        all_price_car: all_price_car,
                        all_test_price_car: all_test_price_car,
                        RoutePolicy: RoutePolicy,
                        car_seat: jsonString
                    },
                    success: function (response) {
                        jQuery('#' + car_html_id).html(response);
                        // jQuery('#back_order_buton1').css('display', 'none')
                        // jQuery('#back_order_buton2').css('display', 'inline-block')
                        // alert(Cookies.get('direct_data'));
                    }
                });
            }

            // Это условие если отмечен чекбокс обратно, но мы уже выбирали билеты туда, а сейчас выбираем обратно (то есть предыдущее условие уже срабатывало)
            if (jQuery('#DirToggle').prop('checked') && localStorage.getItem('direct_data') == 1) {
                jQuery.ajax({
                    url: window.ajaxUrl,
                    method: 'post',
                    data: {
                        action: 'ajax_to_seat',
                        only_direct: 1,
                        count_adult: count_adult,
                        count_children: count_children,
                        count_baby: count_baby,
                        button_id: car_html_id,
                        Origin_name: Origin_name,
                        Origin_id: Destination_id,
                        Destination_name: Destination_name,
                        Destination_id: Origin_id,
                        DepartureDateDirect: DepartureDateDirect,
                        DepartureDateReturn: DepartureDateReturn,
                        service_prov_input: service_prov_input,
                        CarType: CarType,
                        train_LocalDepartureTime: train_LocalDepartureTime,
                        train_LocalDepartureDate: train_LocalDepartureDate,
                        train_OriginStation_name: train_OriginStation_name,
                        train_LocalArrivalTime: train_LocalArrivalTime,
                        train_LocalArrivalDate: train_LocalArrivalDate,
                        train_DestinationStation_name: train_DestinationStation_name,
                        ServiceClass: ServiceClass,
                        Type_coupe: sel3,
                        Jarus: sel4,
                        Type_seat: sel5,
                        all_price_car: all_price_car,
                        all_test_price_car: all_test_price_car,
                        RoutePolicy: RoutePolicy,
                        car_seat: jsonString,
                        car_seat_direct: localStorage.getItem('direct_seat')
                    },
                    success: function (response) {
                        jQuery('#' + car_html_id).html(response);
                    }
                });
            }

            // Это условие срабатывает в любом случае если чекбокс обратно НЕ отмечен
            if (jQuery('#DirToggle').prop('checked') == false) {
                jQuery.ajax({
                    url: window.ajaxUrl,
                    method: 'post',
                    data: {
                        action: 'ajax_to_seat',
                        only_direct: 1,
                        count_adult: count_adult,
                        count_children: count_children,
                        count_baby: count_baby,
                        button_id: car_html_id,
                        Origin_name: Origin_name,
                        Origin_id: Origin_id,
                        Destination_name: Destination_name,
                        Destination_id: Destination_id,
                        DepartureDateDirect: DepartureDateDirect,
                        DepartureDateReturn: DepartureDateReturn,
                        service_prov_input: service_prov_input,
                        CarType: CarType,
                        train_LocalDepartureTime: train_LocalDepartureTime,
                        train_LocalDepartureDate: train_LocalDepartureDate,
                        train_OriginStation_name: train_OriginStation_name,
                        train_LocalArrivalTime: train_LocalArrivalTime,
                        train_LocalArrivalDate: train_LocalArrivalDate,
                        train_DestinationStation_name: train_DestinationStation_name,
                        ServiceClass: ServiceClass,
                        Type_coupe: sel3,
                        Jarus: sel4,
                        Type_seat: sel5,
                        all_price_car: all_price_car,
                        all_test_price_car: all_test_price_car,
                        RoutePolicy: RoutePolicy,
                        car_seat: jsonString
                    },
                    success: function (response) {
                        jQuery('#' + car_html_id).html(response);
                    }
                });
            }
        }
    });

    // Выбор места на схеме

    // Клик по активному месту
    jQuery(".t_place_active").live('click', function () {

        var Origin_name = jQuery("#name0").val();
        var Origin_id = jQuery("#name0_0").val();
        var Destination_name = jQuery("#name1").val();
        var Destination_id = jQuery("#name1_1").val();
        var train_LocalDepartureTime = jQuery("#LocalDepartureTime").val();
        var train_LocalDepartureDate = jQuery("#LocalDepartureDate").val();
        var train_OriginStation_name = jQuery("#OriginStation_name").val();
        var train_LocalArrivalTime = jQuery("#LocalArrivalTime").val();
        var train_LocalArrivalDate = jQuery("#LocalArrivalDate").val();
        var train_DestinationStation_name = jQuery("#DestinationStation_name").val();
        var RoutePolicy = jQuery("#RoutePolicy").val();
        var DepartureDateDirect = jQuery("#date0").val() + 'T00:00:00';

        if (jQuery('#DirToggle').prop('checked')) {
            var DepartureDateReturn = jQuery("#date1").val() + 'T00:00:00';
        } else {
            var DepartureDateReturn = false;
        }

        var CarType = jQuery('#CarType').val();
        var ServiceClass = jQuery('#ServiceClass').val();
        var service_prov_input = jQuery('#service_prov_input').val();
        var car_seat = parseInt(jQuery(this).attr('data-seat'));
        var count_adult = parseInt(jQuery('.adult .adult-num').val());
        var count_children = parseInt(jQuery('.childrensLess10 .less10').val());
        var count_baby = parseInt(jQuery('.childrensLess5 .less5').val());
        var seats_number = count_adult + count_children + count_baby;

        const test_price_car = jQuery("#test_price_car").val();
        const min_price_car = jQuery("#min_price_car").val();
        const seats_number_no_baby = count_adult + count_children;
        const all_price_car = seats_number_no_baby * min_price_car;
        const all_test_price_car = seats_number_no_baby * test_price_car;

        var car_html_id = jQuery(this).data('train');

        if (saetArr.length < 2) {
            // Проверка на количество элементов в массиве если их больше чем 2 тогда массив обнуляется
            saetArr.push(car_seat);
            jQuery(this).addClass("t_place_select");
            if (saetArr[0] < saetArr[1]) {
                for (var i = saetArr[0]; i <= saetArr[1]; i++) {
                    jQuery(".t_place_active[data-seat=" + i + "]").addClass("t_place_select");
                }
            }
            if (saetArr[0] > saetArr[1]) {
                for (var i = saetArr[1]; i <= saetArr[0]; i++) {
                    jQuery(".t_place_active[data-seat=" + i + "]").addClass("t_place_select");
                }
            }
        } else {
            // Обнуляем массив и записываем туда новые значения
            jQuery(".t_place_active").removeClass("t_place_select");
            saetArr = [];
            jQuery(this).addClass("t_place_select");
            saetArr.push(car_seat)
        }

        if (saetArr.length === 2 || seats_number === 1) {
            //  Выводит массив с 2-мя значениями первое выбранное и второе выбранное место
            var jsonString = JSON.stringify(saetArr);

            // Это условие если отмечен чекбокс обратно, но мы ещё не выбирали обратные билеты (то есть это условие ещё не срабатывало)
            if (jQuery('#DirToggle').prop('checked') && localStorage.getItem('direct_data') == undefined) {
                jQuery.ajax({
                    url: window.ajaxUrl,
                    method: 'post',
                    data: {
                        action: 'ajax_to_seat',
                        only_direct: 0,
                        count_adult: count_adult,
                        count_children: count_children,
                        count_baby: count_baby,
                        button_id: car_html_id,
                        Origin_name: Origin_name,
                        Origin_id: Origin_id,
                        Destination_name: Destination_name,
                        Destination_id: Destination_id,
                        DepartureDateDirect: DepartureDateDirect,
                        DepartureDateReturn: DepartureDateReturn,
                        service_prov_input: service_prov_input,
                        CarType: CarType,
                        train_LocalDepartureTime: train_LocalDepartureTime,
                        train_LocalDepartureDate: train_LocalDepartureDate,
                        train_OriginStation_name: train_OriginStation_name,
                        train_LocalArrivalTime: train_LocalArrivalTime,
                        train_LocalArrivalDate: train_LocalArrivalDate,
                        train_DestinationStation_name: train_DestinationStation_name,
                        ServiceClass: ServiceClass,
                        all_price_car: all_price_car,
                        RoutePolicy: RoutePolicy,
                        all_test_price_car: all_test_price_car,
                        car_seat: jsonString
                    },
                    success: function (response) {
                        jQuery('#' + car_html_id).html(response);
                        // jQuery('#back_order_buton1').css('display', 'none')
                        // jQuery('#back_order_buton2').css('display', 'inline-block')
                        // alert(Cookies.get('direct_data'));
                    }
                });
            }

            // Это условие если отмечен чекбокс обратно, но мы уже выбирали билеты туда, а сейчас выберем обратно (то есть предыдущее условие уже срабатывало)
            if (jQuery('#DirToggle').prop('checked') && localStorage.getItem('direct_data') == 1) {
                jQuery.ajax({
                    url: window.ajaxUrl,
                    method: 'post',
                    data: {
                        action: 'ajax_to_seat',
                        only_direct: 1,
                        count_adult: count_adult,
                        count_children: count_children,
                        count_baby: count_baby,
                        button_id: car_html_id,
                        Origin_name: Origin_name,
                        Origin_id: Destination_id,
                        Destination_name: Destination_name,
                        Destination_id: Origin_id,
                        DepartureDateDirect: DepartureDateDirect,
                        DepartureDateReturn: DepartureDateReturn,
                        service_prov_input: service_prov_input,
                        CarType: CarType,
                        train_LocalDepartureTime: train_LocalDepartureTime,
                        train_LocalDepartureDate: train_LocalDepartureDate,
                        train_OriginStation_name: train_OriginStation_name,
                        train_LocalArrivalTime: train_LocalArrivalTime,
                        train_LocalArrivalDate: train_LocalArrivalDate,
                        train_DestinationStation_name: train_DestinationStation_name,
                        ServiceClass: ServiceClass,
                        all_price_car: all_price_car,
                        RoutePolicy: RoutePolicy,
                        all_test_price_car: all_test_price_car,
                        car_seat: jsonString,
                        car_seat_direct: localStorage.getItem('direct_seat')
                    },
                    success: function (response) {
                        jQuery('#' + car_html_id).html(response);
                    }
                });
            }

            // Это условие срабатывает в любом случае если чекбокс обратно НЕ отмечен
            if (jQuery('#DirToggle').prop('checked') == false) {
                jQuery.ajax({
                    url: window.ajaxUrl,
                    method: 'post',
                    data: {
                        action: 'ajax_to_seat',
                        only_direct: 1,
                        count_adult: count_adult,
                        count_children: count_children,
                        count_baby: count_baby,
                        button_id: car_html_id,
                        Origin_name: Origin_name,
                        Origin_id: Origin_id,
                        Destination_name: Destination_name,
                        Destination_id: Destination_id,
                        DepartureDateDirect: DepartureDateDirect,
                        DepartureDateReturn: DepartureDateReturn,
                        service_prov_input: service_prov_input,
                        CarType: CarType,
                        train_LocalDepartureTime: train_LocalDepartureTime,
                        train_LocalDepartureDate: train_LocalDepartureDate,
                        train_OriginStation_name: train_OriginStation_name,
                        train_LocalArrivalTime: train_LocalArrivalTime,
                        train_LocalArrivalDate: train_LocalArrivalDate,
                        train_DestinationStation_name: train_DestinationStation_name,
                        ServiceClass: ServiceClass,
                        all_price_car: all_price_car,
                        RoutePolicy: RoutePolicy,
                        all_test_price_car: all_test_price_car,
                        car_seat: jsonString
                    },
                    success: function (response) {
                        jQuery('#' + car_html_id).html(response);
                    }
                });
            }
        }

    });

    // Отправка запроса форма 1 (запрос обратных поездов)
    jQuery("#back_order_buton2").live('click', function (event) {
        event.preventDefault();

        jQuery('#to_result_html').html('Идёт поиск подходящих маршрутов');

        var direct_seat = jQuery(this).data('direct_seat');

        localStorage.setItem('direct_seat', direct_seat);
        localStorage.setItem('direct_data', 1);

        var Origin_0 = jQuery("#name0").val();
        var Destination_0 = jQuery("#name1").val();

        jQuery("#name0").val(Destination_0);
        jQuery("#name1").val(Origin_0);

        var Origin = jQuery("#name1_1").val();
        var Destination = jQuery("#name0_0").val();

        var DepartureDate = jQuery("#date1").val() + 'T00:00:00';
        var TimeFrom = 0;
        var TimeTo = 24;

        jQuery.ajax({
            url: window.ajaxUrl,
            method: 'post',
            data: {
                action: 'ajax_to_trainpricing',
                Origin: Origin,
                Destination: Destination,
                DepartureDate: DepartureDate,
                TimeFrom: TimeFrom,
                direct: 0,
                TimeTo: TimeTo
            },
            success: function (response) {
                jQuery('html, body').animate({
                    scrollTop: jQuery('#to_result_html').offset().top - 150
                }, 500);
                jQuery('#to_result_html').html(response);
            }
        });
    });

    // Табы переключения между состояниями в выборе билетов (выбор по параметрам и выбор на схеме)
    jQuery(".tabs_change_sits_parameters .link").live('click', function () {
        var thisId = jQuery(this).attr('id');
        jQuery('.tabs_change_sits_parameters .link').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.tab_content_item').removeClass('active');
        jQuery('.tab_content_item[data-tab=' + thisId + ']').addClass('active');
    });

});
