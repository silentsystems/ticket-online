/**
 * This file is part of is free software.
 */

jQuery(document).ready(function () {
    jQuery('.pop_circle').live('click', function () {
        var thisDataInfo = jQuery(this).data('popup_day');
        console.log(thisDataInfo);
        jQuery('.calendar_item_popup').stop(false, true).fadeIn(300).attr('id', '' + thisDataInfo + '')
    });
    jQuery(document).on('click', '.calendar_bg_dark, .close_calendar_popup', function () {
        jQuery('.calendar_item_popup').stop(false, true).fadeOut(200).attr('id', '')
    });
});
