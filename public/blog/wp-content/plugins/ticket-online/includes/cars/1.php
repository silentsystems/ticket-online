<?php

/**
 * This file is part of is free software.
 */

/**
 * Created on: 05.05.2019, 13:50:10
 * Author: Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

$arr = [];

if (isset($car_arr)) {
    foreach ($car_arr as $car) {
        $arr[] = $car ["FreePlaces"];
    }
}

$comma_separated = preg_replace('/[^0-9 ,]/', '', implode(",", $arr));

$seat = explode(",", $comma_separated);

if ($car_arr[0]["CarPlaceType"] == 'Lower') {
    $price_up = $car_arr[0]["MinPrice"];
    $price_down = $car_arr[1]["MinPrice"];
}

if ($car_arr[0]["CarPlaceType"] == 'Upper') {
    $price_up = $car_arr[1]["MinPrice"];
    $price_down = $car_arr[0]["MinPrice"];
}

$up_prices = ($price_up / 100) * $to_cost;
$up_prices = round($price_up + $up_prices, 1);

$down_prices = ($price_down/ 100) * $to_cost;
$down_prices = round($price_down + $down_prices, 1);

?>

<div class="to-carousel__slide to-carousel__slide_selected">
    <div class="to-regular">
        <div class="to-regular__cabin">
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place">
                        2
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        1
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        4
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        3
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side to-regular__berth_side_disabled">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        54
                        <span class="to-regular__place-tip">Верхнее боковое</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        53
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        6
                        <span class="to-regular__place-tip">Верхнее</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        5
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        8
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        7
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side to-regular__berth_side_disabled">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        52
                        <span class="to-regular__place-tip">Верхнее боковое</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        51
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        10
                        <span class="to-regular__place-tip">Верхнее</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        9
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        12
                        <span class="to-regular__place-tip">Верхнее</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        11
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        50
                        <span class="to-regular__place-tip">Верхнее боковое</span>
                    </span>
                    <span class="to-regular__place t_place">
                        49
                        <span class="to-regular__place-tip">Нижнее боковое,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place">
                        14
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        13
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        16
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        15
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side to-regular__berth_side_disabled">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        48
                        <span class="to-regular__place-tip">Верхнее боковое</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        47
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        18
                        <span class="to-regular__place-tip">Верхнее</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        17
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        20
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        19
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side">
                    <span class="to-regular__place t_place">
                        46
                        <span class="to-regular__place-tip">
                            Верхнее боковое,
                            <span class="to-regular__place-price">
                                1 323,9
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        45
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place">
                        22
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        21
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        24
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        23
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side">
                    <span class="to-regular__place t_place">
                        44
                        <span class="to-regular__place-tip">
                            Верхнее боковое,
                            <span class="to-regular__place-price">
                                1 323,9
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        43
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place">
                        26
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        25
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        28
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        27
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side">
                    <span class="to-regular__place t_place">
                        42
                        <span class="to-regular__place-tip">
                            Верхнее боковое,
                            <span class="to-regular__place-price">
                                1 323,9
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        41
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place">
                        30
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        29
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        32
                        <span class="to-regular__place-tip">Верхнее</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        31
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side">
                    <span class="to-regular__place t_place">
                        40
                        <span class="to-regular__place-tip">
                            Верхнее боковое,
                            <span class="to-regular__place-price">
                                1 323,9
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        39
                        <span class="to-regular__place-tip">Нижнее боковое</span>
                    </span>
                </div>
            </div>
            <div class="to-regular__section">
                <div class="to-regular__berth to-regular__berth_left">
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        34
                        <span class="to-regular__place-tip">Верхнее</span>
                    </span>
                    <span class="to-regular__place t_place to-regular__place_disabled">
                        33
                        <span class="to-regular__place-tip">Нижнее</span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_right">
                    <span class="to-regular__place t_place">
                        36
                        <span class="to-regular__place-tip">
                            Верхнее,
                            <span class="to-regular__place-price">
                                1 323,9
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place">
                        35
                        <span class="to-regular__place-tip">
                            Нижнее,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-regular__berth to-regular__berth_side">
                    <span class="to-regular__place t_place">
                        38
                        <span class="to-regular__place-tip">
                            Верхнее боковое,
                            <span class="to-regular__place-price">
                                1 323,9
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="to-regular__place t_place">
                        37
                        <span class="to-regular__place-tip">
                            Нижнее боковое,
                            <span class="to-regular__place-price">
                                1 977,6
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
