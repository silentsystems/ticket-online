<?php

/**
 * This file is part of is free software.
 */

/**
 * Created on: 05.05.2019, 13:50:10
 * Author: Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

$arr = [];

if (isset($car_arr)) {
    foreach ($car_arr as $car) {
        $arr[] = $car ["FreePlaces"];
    }
}

$comma_separated = preg_replace('/[^0-9 ,]/', '', implode(",", $arr));

$seat = explode(",", $comma_separated);

if ($car_arr[0]["CarPlaceType"] == 'Lower') {
    $price_up = $car_arr[0]["MinPrice"];
    $price_down = $car_arr[1]["MinPrice"];
}

if ($car_arr[0]["CarPlaceType"] == 'Upper') {
    $price_up = $car_arr[1]["MinPrice"];
    $price_down = $car_arr[0]["MinPrice"];
}

$up_prices = ($price_up / 100) * $to_cost;
$up_prices = round($price_up + $up_prices, 1);

$down_prices = ($price_down/ 100) * $to_cost;
$down_prices = round($price_down + $down_prices, 1);

?>

<div class="to-carousel__slide to-carousel__slide_selected">
    <div class="to-fpc to-fpc_6">
        <div class="to-fpc__cabin">
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        1
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        2
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        3
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        4
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                        5
                        <span class="to-fpc__berth-place-tip"></span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                        6
                        <span class="to-fpc__berth-place-tip"></span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                        7
                        <span class="to-fpc__berth-place-tip"></span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                        8
                        <span class="to-fpc__berth-place-tip"></span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        9
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        10
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        11
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        12
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        13
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        14
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        15
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        16
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
            <div class="to-fpc__section">
                <div class="to-fpc__berth to-fpc__berth_left">
                    <span class="to-fpc__berth-place t_place">
                        17
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <div class="to-fpc__berth to-fpc__berth_right">
                    <span class="to-fpc__berth-place t_place">
                        18
                        <span class="to-fpc__berth-place-tip">
                            <span class="to-fpc__berth-place-price">
                                7 747
                                <span class="to-rubl">
                                    R
                                    <span class="is-hidden">руб.</span>
                                </span>
                            </span>
                        </span>
                    </span>
                </div>
                <span class="to-places-scheme__gender"></span>
            </div>
        </div>
    </div>
</div>
