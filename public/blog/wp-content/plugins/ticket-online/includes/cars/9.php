<?php

/**
 * This file is part of is free software.
 */

/**
 * Created on: 05.05.2019, 13:50:10
 * Author: Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

$arr = [];

$m = false; // муж
$g = false; // жен
$c = false; // целое
$smesh = false;  // смешанное

if (isset($car_arr)) {
    foreach ($car_arr as $car) {
        $arr[] = $car ["FreePlaces"];
        if (stripos($car["FreePlaces"], 'М')) {
            $m = true;
        }
        if (stripos($car["FreePlaces"], 'Ж')) {
            $g = true;
        }
        if (stripos($car["FreePlaces"], 'Ц')) {
            $c = true;
        }
        if (stripos($car["FreePlaces"], 'С')) {
            $smesh = true;
        }
    }
}

$comma_separated = preg_replace('/[^0-9 ,]/', '', implode(",", $arr));

$seat = explode(",", $comma_separated);

if ($car_arr[0]["CarPlaceType"] == 'Lower') {
    $price_up = $car_arr[0]["MinPrice"];
    $price_down = $car_arr[1]["MinPrice"];
}

if ($car_arr[0]["CarPlaceType"] == 'Upper') {
    $price_up = $car_arr[1]["MinPrice"];
    $price_down = $car_arr[0]["MinPrice"];
}

$nacenka = 0;

foreach ($to_cost as $key => $value) {
    if ((int) $key > $price_up) {
        $nacenka = (int) $value;
        break;
    }
}

$up_prices = ($price_up / 100) * $nacenka;
$up_prices = round($price_up + $up_prices, 1);

$nacenka = 0;

foreach ($to_cost as $key => $value) {
    if ((int) $key > $price_down) {
        $nacenka = (int) $value;
        break;
    }
}

$down_prices = ($price_down / 100) * $nacenka;
$down_prices = round($price_down + $down_prices, 1);

if (!isset($car_html_id)) {
    $car_html_id = 1;
}

?>

<div class="train_cell_cars_in_circuit tab_content_item active" data-tab="sits">
    <div class="to-carousel__slide to-carousel__slide_selected">
        <div class="to-fpc to-fpc_9">
            <div class="to-fpc__cabin">
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("38", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                38
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="38">
                                38
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("37", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                37
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="37">
                                37
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("2", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                2
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="2">
                                2
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("1", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                1
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-seat="1">
                                1
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("4", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                4
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="4">
                                4
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("3", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                3
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="3">
                                3
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][0]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][0]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][0]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][0]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][0]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][0]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][0]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][0]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("6", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                6
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="6">
                                6
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("5", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                5
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="5">
                                5
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("8", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                8
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="8">
                                8
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("7", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                7
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="7">
                                7
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][1]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][1]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][1]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][1]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][1]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][1]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][1]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][1]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("10", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                10
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="10">
                                10
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("9", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                9
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="9">
                                9
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("12", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                12
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="12">
                                12
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("11", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                11
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="11">
                                11
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][2]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][2]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][2]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][2]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][2]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][2]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][2]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][2]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("14", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                14
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="14">
                                14
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("13", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                13
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="13">
                                13
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("16", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                16
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="16">
                                16
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("15", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                15
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="15">
                                15
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][3]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][3]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][3]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][3]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][3]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][3]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][3]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][3]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("18", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                18
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="18">
                                18
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("17", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                17
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="17">
                                17
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("20", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                20
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="20">
                                20
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("19", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                19
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="19">
                                19
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][4]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][4]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][4]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][4]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][4]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][4]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][4]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][4]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("22", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                22
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="22">
                                22
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("21", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                21
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="21">
                                21
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("24", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                24
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="24">
                                24
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("23", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                23
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="23">
                                23
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][5]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][5]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][5]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][5]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][5]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][5]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][5]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][5]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("26", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                26
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="26">
                                26
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("25", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                25
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="25">
                                25
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("28", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                28
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="28">
                                28
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("27", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                27
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="27">
                                27
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][6]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][6]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][6]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][6]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][6]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][6]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][6]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][6]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("30", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                30
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="30">
                                30
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("29", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                29
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="29">
                                29
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("32", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                32
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="32">
                                32
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("31", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                31
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="31">
                                31
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][7]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][7]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][7]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][7]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][7]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][7]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][7]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][7]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
                <div class="to-fpc__section">
                    <div class="to-fpc__berth to-fpc__berth_left">
                        <?php if (!in_array("34", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                34
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="34">
                                34
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("33", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                33
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="33">
                                33
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <div class="to-fpc__berth to-fpc__berth_right">
                        <?php if (!in_array("36", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                36
                                <span class="to-fpc__berth-place-tip">Верхнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="36">
                                36
                                <span class="to-fpc__berth-place-tip">
                                    Верхнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $up_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                        <?php if (!in_array("35", $seat)) { ?>
                            <span class="to-fpc__berth-place t_place to-fpc__berth-place_disabled">
                                35
                                <span class="to-fpc__berth-place-tip">Нижнее</span>
                            </span>
                        <?php } else { ?>
                            <span class="to-fpc__berth-place t_place t_place_active" data-train="<?php echo $car_html_id; ?>" data-seat="35">
                                35
                                <span class="to-fpc__berth-place-tip">
                                    Нижнее,
                                    <span class="to-fpc__berth-place-price">
                                        <?php echo $down_prices; ?>
                                        <span class="to-rubl">
                                            <span class="is-hidden">руб.</span>
                                        </span>
                                    </span>
                                </span>
                            </span>
                        <?php } ?>
                    </div>
                    <span class="to-places-scheme__gender">
                        <?php
                        $geder = '';
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][8]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][8]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][8]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[0]["FreePlacesByCompartments"][8]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][8]["Places"], 'Ц')) {
                            $geder = 'Ц';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][8]["Places"], 'М')) {
                            $geder = 'М';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][8]["Places"], 'Ж')) {
                            $geder = 'Ж';
                        }
                        if (strpos($car_arr[1]["FreePlacesByCompartments"][8]["Places"], 'С')) {
                            $geder = 'С';
                        }
                        echo $geder;
                        ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <?php if ($car_arr[0]["HasGenderCabins"] == true || $car_arr[1]["HasGenderCabins"] == true) { ?>
        <div class="to-legend-info to-legend-info_inline">
            <div class="to-legend-info__item to-legend-info__item_odd">
                М — мужское купе
            </div>
            <div class="to-legend-info__item to-legend-info__item_even">
                Ж — женское купе
            </div>
            <div class="to-legend-info__item to-legend-info__item_odd">
                С — смешанное купе
            </div>
            <div class="to-legend-info__item to-legend-info__item_even">
                Ц — целое купе
            </div>
        </div>
    <?php } ?>
</div>
<div class="train_cell_cars_in_selects tab_content_item" data-tab="params">
    <label for="select1">От</label>
    <select name="select1" id="select1">
        <?php
        sort($seat);
        foreach ($seat as $s) {
        ?>
            <option value="<?php echo (int) $s; ?>"><?php echo (int) $s; ?></option>
        <?php } ?>
    </select>
</div>
<label for="select2">До</label>
<select name="select2" id="select2">
    <?php
    sort($seat);
    foreach ($seat as $s) {
    ?>
        <option value="<?php echo (int) $s; ?>"><?php echo (int) $s; ?></option>
    <?php } ?>
</select>
<br>
<br>
<?php if ($m OR $g OR $c OR $smesh) { ?>
<label for="select3">Тип купе</label>
<select name="select3" id="select3">
    <?php
    if ($m) {
        ?>
        <option value="man">Мужское</option>
        <?php
    }
    if ($g) {
        ?>
        <option value="woman">Женское</option>
        <?php
    }
    if ($c) {
        ?>
        <option value="complite">Целое</option>
        <?php
    }
    if ($smesh) {
        ?>
        <option value="separate">Смешанное</option>
    <?php
    } ?>
</select>
<?php } ?>
<label for="select4">Ярус</label>
<select name="select4" id="select4">
    <option value="up">Верхний</option>
    <option value="down">Нижний</option>
</select>
<label for="select5">Тип места</label>
<select name="select5" id="select5">
    <option>Выберите</option>
    <option value="one_coupe">В одном купе</option>
</select>
<input type="hidden" id="select_car_html_id"  value="<?php echo $car_html_id; ?>">
