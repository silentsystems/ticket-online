<?php

/*
 * This file is part of is free software.
 */
/*
  Created on : 09.04.2019, 12:24:05
  Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */


if (!defined('WPINC')) {
    die;
}

### Function: add shortcode  личный кабинет

function to_customer_funcion() {
    $authdata = base64_encode(get_option('to_username') . ":" . get_option('to_password'));
    $host_api = get_option('to_host_api');
    $param_pos = get_option('to_param_pos');

    $out_html = '';
    $out_html .= '<div class="to_content">';

    $out_html .= '
    
      <table class="customer_data">
        <thead class="costomer_table_head">
          <tr>
            <th>#</th>
            <th>Дата заказа</th>
            <th>Данные о заказе <br>(номер вагона и места)</th>
            <th>Сумма заказа</th>
            <th>Статус заказа</th>
            <th>Примечание</th>
          </tr>
        </thead>
        <tbody class="costomer_table_body">
          <tr>
            <td>1</td>
            <td>01.01.19</td>
            <td>Вагон 10, место 15 Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. </td>
            <td>3892р.</td>
            <td>В ожжидании</td>
            <td>Lorem ipsum dolor sit amet.</td>
          </tr>
          <tr>
            <td>2</td>
            <td>01.01.19</td>
            <td>Вагон 10, место 15</td>
            <td>3892р.</td>
            <td>В ожжидании</td>
            <td>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. </td>
          </tr>
          <tr>
            <td>3</td>
            <td>01.01.19</td>
            <td>Вагон 10, место 15</td>
            <td>3892р.</td>
            <td>В ожжидании</td>
            <td></td>
          </tr>
          <tr>
            <td>4</td>
            <td>01.01.19</td>
            <td>Вагон 10, место 15</td>
            <td>3892р.</td>
            <td>В ожжидании</td>
            <td>Lorem ipsum dolor sit amet.</td>
          </tr>
          <tr>
            <td>5</td>
            <td>01.01.19</td>
            <td>Вагон 10, место 15</td>
            <td>3892р.</td>
            <td>В ожжидании</td>
            <td></td>
          </tr>
        </tbody>
      </table>

    ';

    
    $out_html .= '</div>';






    return $out_html;
}

add_shortcode('to_customer', 'to_customer_funcion');



