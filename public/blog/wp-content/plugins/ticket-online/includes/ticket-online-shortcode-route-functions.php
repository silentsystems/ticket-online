<?php
/*
 * This file is part of is free software.
 */
/*
  Created on : 11.04.2019, 13:08:11
  Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

if (!defined('WPINC')) {
    die;
}
### Function: add shortcode вывода маршрутов

function to_funcion_route() {


    if (isset($_GET['TrainNumber']) && isset($_GET['Origin']) && isset($_GET['Destination']) && isset($_GET['DepartureDate'])) {

        $username = get_option('to_username');
        $password = get_option('to_password');
        $host_api = get_option('to_host_api');
        $param_pos = get_option('to_param_pos');
        $authdata = base64_encode($username . ":" . $password);

        $LocalDepartureDateTime = strtotime($_GET['DepartureDate']);
        $LocalDepartureTime = date("H:i", $LocalDepartureDateTime);

        $LocalDepartureDate = date("d.m.Y", $LocalDepartureDateTime);

        ### Получение маршрута следования поезда
        $api_method = 'Railway/V1/Search/TrainRoute';
        $data = array(
            "TrainNumber" => trim($_GET['TrainNumber']),
            "Origin" => trim($_GET['Origin']),
            "Destination" => trim($_GET['Destination']),
            "DepartureDate" => trim($_GET['DepartureDate']),
        ); // data u want to post

        $data_string = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $authdata,
            'Accept: application/json',
            'Content-Type: application/json',
            'cache-control: no-cache',
            'Content-Encoding: gzip',
            'pos: ' . $param_pos
                )
        );

        $return = '';

        if (curl_exec($ch) === false) {
            $return = 'Curl error: ' . curl_error($ch);
        }
        $errors = curl_error($ch);
        $result = curl_exec($ch);
        $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $out = json_decode($result, true);

        $out_html = '';

        $out_html .= '<div class="to_content">';

        if (isset($out["Code"])) {
            ?>

            <div class="error_message" > <?php echo $out["Message"]; ?> </div>

            <?php
        } elseif ($out) {



            $out_html .= '<h3><span>Маршрут поезда №</span> <b>' . trim($_GET['TrainNumber']) . ', ' . $out["Routes"][0]["OriginName"] . ' — ' . $out["Routes"][0]["DestinationName"] . ' '
                    . '</b> на дату отправления  <b>' . $LocalDepartureDate . '</b></h3>';
            $out_html .= '<h4>Указано время станций отправления</h4>';
            $out_html .= '<div class="route_table">';



            $out_html .= '<table>
                            <thead>
		<tr>
			<th scope="col">
				Станция</th>
			<th scope="col">
				Время прибытия</th>
			<th scope="col">
				Время стоянки</th>
			<th scope="col">
				Время отправления</th>
		</tr>
	</thead>';
            $out_html .= '<tbody>';
            foreach ($out["Routes"][0]["RouteStops"] as $data) {

                $out_html .= '<tr>';
                $out_html .= '<td>' . $data["StationName"] . '</td>';
                $out_html .= '<td>' . $data["LocalArrivalTime"] . '</td>';
                $out_html .= '<td>';
                if ((int) $data["StopDuration"] > 0) {
                    $out_html .= $data["StopDuration"] . ' мин.';
                }
                $out_html .= '</td>';
                $out_html .= '<td>' . $data["LocalDepartureTime"] . '</td>';
                $out_html .= '</tr>';
            }

            $out_html .= '</tbody>';
            $out_html .= '</table>';

            $out_html .= '</div>';
        } else {


            $out_html .= '<div class="train_error"> Не удалось получить данные!</div>';
        }

        $out_html .= '</div>';
    } else {

        $out_html = '';

        $out_html .= '<div class="to_content">';

        $out_html .= '<div class="error_message" >Данные указаны не верно. Нет такого маршрута.</div>';

        $out_html .= '</div>';
    }

    return $out_html;
}

add_shortcode('to_route', 'to_funcion_route');



