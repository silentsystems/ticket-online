<?php

/*
 * This file is part of is free software.
 */
/*
  Created on : 09.04.2019, 12:24:05
  Author : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */


if (!defined('WPINC')) {
die;
}

### Function: add shortcode  запроса билетов 

function to_funcion() {
$authdata = base64_encode(get_option('to_username') . ":" . get_option('to_password'));
$host_api = get_option('to_host_api');
$param_pos = get_option('to_param_pos');

$out_html = '';
$out_html .= '<div class="to_content">'; 

$out_html .= '<div class="to_tabs">
<input id="tab1" type="radio" name="tabs" checked><label for="tab1" title="Купить билет">Купить билет
</br>и выбрать поезд</label><input id="tab4" type="radio" name="tabs"><label for="tab4" title="Базовое расписание дальнего следования">Базовое расписание
</br>дальнего следования</label>';

$out_html .= '<section id="content-tab1"> 
  
 <form class="train_pricing_form" name="TrainPricing" id="TrainPricingForm">
 <div class="left">
  <p>Станции отправления и прибытия</p>
  <p>
  <input class="name0_start" id="name0" type="text" name="st0" value="" autocomplete="off" placeholder="Откуда">
  <input   id="name0_0" type="hidden" name="st0_0" value="">
  <ul class="name0_start-search"></ul>
  <input  class="name1_end" id="name1" type="text" name="st1" value="" autocomplete="off" placeholder="Куда">
  <input   id="name1_1" type="hidden" name="st1_1" value="">
  <ul class="name1_end-search"></ul>
  </p>
  <div class="tickets-number">
<input type="hidden" class="all_inp_sum" value="0">
  <div class="number-container adult">
<p>взрослые</p>
<input type="number" class="adult-num numb" id="adult-num"  min="1" max="4" name="adult-num" value="1">
  </div>
  <div class="number-container childrensLess10">
<p >дети до 10 лет</p>
<input type="number" class="less10 numb" name="less10"  min="0" max="3" id="less10" value="0">
  </div>
  <div class="number-container childrensLess5">
<p>дети до 5 лет (без места)</p>
<input type="number" name="less5" class="less5 numb" min="0" max="2" id="less5" value="0">
  </div>
  </div>

  </div>
  <div class="right">
  <p>Дата и время отправления</p>
<p>
 <label class="right_label">

  </label>
   <input id="date0" type="text" placeholder="Выберите дату">
   <label class="right_label">
  <input id="DirToggle" type="checkbox">
Обратно
  </label>
  <input id="date1" type="text" placeholder="Выберите дату" disabled>
  </p>
  </br></br>
   <div class="left left2">

  </div>
  <div class="right right2">
  <p>

 
<button id="Submit1" type="submit" >Расписание</button>
</p>
</div>
</div>

<div class="clearfix"></div>


 </form>

<div class="links">
<div class="link_1">
<a href="#">Оформить заявку на перевозку автомобиля</a>
</div>

<div class="link_2">
<a href="#">Деловой проездной</a>
</div>

<div class="link_3">
<a href="#">Подарочная карта</a>
</div>

<div class="link_4">
<a href="#">Оферта</a>
</div>

<div class="link_5">
<a href="#">Купить премиальный билет</a>
</div>
</div>
</section>';
  //  $out_html .= '<section id="content-tab2">
   // <p>
  //Здесь любое содержание....
  //  </p>
  //  </section>';
   
$out_html .= '<section id="content-tab4">
<p>
  Здесь любое содержание....
</p>
</section>';
$out_html .= '</div>';

$out_html .= '<div id="to_result_html" class="to_result_html"></div>';


// $out_html .= '<div class="to_footer">«С использованием веб-системы «Инновационная мобильность»'
//   . '<img class="logo_im" src="/wp-content/plugins/ticket-online/images/im_logo.png" alt="im_logo"/></div>';

$out_html .= '</div>';




return $out_html;
}

add_shortcode('to_form', 'to_funcion'); 



