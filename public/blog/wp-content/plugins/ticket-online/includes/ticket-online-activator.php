<?php

/*
 * This file is part of is free software.
 */
/*
  Created on : 09.04.2019, 12:03:49
  Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

if (!defined('WPINC')) {
    die;
}

global $wpdb;
$authdata = base64_encode(get_option('to_username') . ":" . get_option('to_password'));
$host_api = get_option('to_host_api');
$param_pos = get_option('to_param_pos');
$site_url = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://';
$site_url = $site_url . $_SERVER['SERVER_NAME'];
update_option('to_secret', base64_encode($site_url));
// таблица для стран
$table_name = $wpdb->get_blog_prefix() . 'to_country';
// проверяем есть ли в базе таблица с таким же именем, если нет - создаем.
if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
// устанавливаем кодировку
    $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";
// подключаем файл нужный для работы с bd
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
// запрос на создание
    $sql = "CREATE TABLE {$table_name} (
  id int(11) NOT NULL AUTO_INCREMENT,
  countryid int(11) NOT NULL,
  alpha2code varchar(10) NOT NULL default '',
  nameru varchar(255) NOT NULL default '',
  nameen varchar(255) NOT NULL default '',
  isactive tinyint(1) NOT NULL,
  updated datetime  NOT NULL,
PRIMARY KEY  (id)
) {$charset_collate};";
// Создать таблицу.
    dbDelta($sql);
}
// заполнить таблицу.
to_country($authdata, $host_api, $param_pos);


// таблица для городов
$table_name = $wpdb->get_blog_prefix() . 'to_city';
// проверяем есть ли в базе таблица с таким же именем, если нет - создаем.
if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
// устанавливаем кодировку
    $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";
// подключаем файл нужный для работы с bd
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
// запрос на создание
    $sql = "CREATE TABLE {$table_name} (
            id int(11) NOT NULL AUTO_INCREMENT,
  cityid int(11) NOT NULL,
  countryid int(11) NOT NULL,
  regionid int(11) NOT NULL,
  syscode varchar(55) NOT NULL default '',
  expresscode varchar(55) NOT NULL default '',
  code varchar(55) NOT NULL default '',
  nameru varchar(255) NOT NULL default '',
  nameen varchar(255) NOT NULL default '',
  popularityindex int(10) NOT NULL ,
  isactive tinyint(1) NOT NULL,
  updated datetime  NOT NULL,
PRIMARY KEY  (id)
) {$charset_collate};";
// Создать таблицу.
    dbDelta($sql);
}
// заполнить таблицу.
to_city($authdata, $host_api, $param_pos);


// таблица для транспортных хабов (nodes)
$table_name = $wpdb->get_blog_prefix() . 'to_nodes';
// проверяем есть ли в базе таблица с таким же именем, если нет - создаем.
if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
// устанавливаем кодировку
    $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset} COLLATE {$wpdb->collate}";
// подключаем файл нужный для работы с bd
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
// запрос на создание


    $sql = "CREATE TABLE {$table_name} (
            id int(11) NOT NULL AUTO_INCREMENT,
  transportnodeid int(55) NOT NULL,
  countryid int(11) NOT NULL,
  regionid int(11) NOT NULL,
  cityid int(11) NOT NULL,
  code varchar(255) NOT NULL default '',
  nameru varchar(512) NOT NULL default '',
  nameen varchar(512) NOT NULL default '',
  type varchar(255) NOT NULL default '',
  popularityindex int(10) NOT NULL ,
  description varchar(512) NOT NULL default '',
  isactive tinyint(1) NOT NULL,
  isvisible tinyint(1) NOT NULL,
  issuburban tinyint(1) NOT NULL,
  updated datetime  NOT NULL,
  utctimeoffset varchar(10) NULL default '',
  location varchar(512) NOT NULL default '',
PRIMARY KEY  (id)
) {$charset_collate};";
// Создать таблицу.
    dbDelta($sql);
}
// заполнить таблицу
to_nodes($authdata, $host_api, $param_pos);


