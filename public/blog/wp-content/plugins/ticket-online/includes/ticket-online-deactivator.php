<?php

if (!defined('WPINC')) {
    die;
}

/* 
 * This file is part of is free software.
 */
/*
    Created on : 09.04.2019, 12:07:39
    Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
*/


    global $wpdb;

    $table_name = $wpdb->get_blog_prefix() . 'to_country';
    $wpdb->query("DROP TABLE ".$table_name);

    $table_name = $wpdb->get_blog_prefix() . 'to_city';
    $wpdb->query("DROP TABLE ".$table_name);

    $table_name = $wpdb->get_blog_prefix() . 'to_nodes';
    $wpdb->query("DROP TABLE ".$table_name);