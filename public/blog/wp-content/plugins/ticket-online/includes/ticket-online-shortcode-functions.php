<?php
/*
 * This file is part of is free software.
 */
/*
  Created on : 11.04.2019, 13:08:11
  Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

function places_ajax_search() {
    global $wpdb;

    if (isset($_POST['term'])) {


        $places = $wpdb->get_results("SELECT `code`,`nameru` FROM `wp_to_nodes` WHERE `nameru` LIKE '" . trim($_POST['term']) . "%' ORDER BY `popularityindex` DESC LIMIT 10");

        if ($places) {

            foreach ($places as $place) {
                ?>



                <li><a data-node_id="<?php echo $place->code; ?>" data-node_nameru="<?php echo $place->nameru; ?>"><?php echo $place->nameru; ?></a></li>

                <?php
            }
        } else {
            ?>
            <li><a data-node_id="null">По вашему запросу ничего не найдено</a></li>
            <?php
        }
    }
    wp_die();
}

add_action('wp_ajax_nopriv_places_ajax_search', 'places_ajax_search');
add_action('wp_ajax_places_ajax_search', 'places_ajax_search');

function ajax_to_trainpricing() {

    $username = get_option('to_username');
    $password = get_option('to_password');
    $host_api = get_option('to_host_api');
    $param_pos = get_option('to_param_pos');
    $authdata = base64_encode($username . ":" . $password);

    $to_route_url = get_option('to_link_to_route_page');


    $api_method = 'Railway/V1/Search/TrainPricing';
    $data = array(
        "Origin" => trim($_POST['Origin']),
        "Destination" => trim($_POST['Destination']),
        "DepartureDate" => trim($_POST['DepartureDate']),
        "TimeFrom" => trim((int) $_POST['TimeFrom']),
        "TimeTo" => trim((int) $_POST['TimeTo']),
        "CarGrouping" => "Group",
        "SpecialPlacesDemand" => "NoValue",
        "GetOnlyCarTransportationCoaches" => false,
        "GetOnlyNonRefundableTariffs" => false
    ); // data u want to post




    $data_string = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $authdata,
        'Accept: application/json',
        'Content-Type: application/json',
        'cache-control: no-cache',
        'Content-Encoding: gzip',
        'pos: ' . $param_pos
            )
    );

    $return = '';

    if (curl_exec($ch) === false) {
        $return = 'Curl error: ' . curl_error($ch);
    }
    $errors = curl_error($ch);
    $result = curl_exec($ch);
    $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $out = json_decode($result, true);



    //var_dump($out); //для отладки ответа


    if ((int) $out["Code"] > 1) {
        ?>

        <div class="error_message" > <?php echo $out["Message"]; ?> </div>

        <?php
    } elseif ($out) {

        $to_cost = get_option('to_cost');

        $RoutePolicy = $out["RoutePolicy"];

        foreach ($out["Trains"] as $data) {
            ?>

            <div class="train_cell">


                <div class="train_cell_1">

                    <div>Перевозчик: <?php
                        if ($data["Carriers"]) {
                            foreach ($data["Carriers"] as $Carriers) {

                                echo $Carriers . ' ';
                            }
                        }
                        ?> по маршруту: <?php echo $data["InitialStationName"]; ?> -> <?php echo $data["FinalStationName"]; ?></div>
                    <div>Поезд №: <?php echo $data["TrainNumber"]; ?> <?php echo $data["TrainName"]; ?></div>
                    <div>Время в пути: <?php
                        $time = $data["TripDuration"];
                        $hours = floor($time / 60);
                        $minutes = $time % 60;
                        printf('%02d:%02d', $hours, $minutes);
                        ?></div>


                    <div><?php
                        $LocalDepartureDateTime = strtotime($data["LocalDepartureDateTime"]);
                        $LocalDepartureTime = date("H:i", $LocalDepartureDateTime);

                        $LocalDepartureDate = date("d.m.Y", $LocalDepartureDateTime);
                        echo 'Отправление: ' . $LocalDepartureTime;
                        ?> (местное)
                        <?php echo $LocalDepartureDate; ?>
                    </div>
                    <div><?php
                        $OriginStation_name = station_name($data["OriginStationCode"]);

                        echo $OriginStation_name;
                        ?></div>

                    <?php
                    if ($data["HasElectronicRegistration"] == null || $data["HasElectronicRegistration"] == '') {
                        echo '«Внимание: Необходимо получить билет в кассе на территории России»';
                    }
                    ?>
                </div>
                <div class="train_cell_2">

                    <div><?php
                        $LocalArrivalDateTime = strtotime($data["LocalArrivalDateTime"]);
                        $LocalArrivalTime = date("H:i", $LocalArrivalDateTime);

                        $LocalArrivalDate = date("d.m.Y", $LocalArrivalDateTime);



                        echo $LocalArrivalTime;
                        ?> (местное) <?php echo $LocalArrivalDate; ?></div>

                    <div><?php
                        $DestinationStation_name = station_name($data["DestinationStationCode"]);

                        echo $DestinationStation_name;
                        ?></div>


                </div>
                <div class="train_cell_3">

                    <a href="<?php echo $to_route_url; ?>?TrainNumber=<?php echo trim($data["TrainNumber"]); ?>&Origin=<?php echo trim($data["OriginStationCode"]); ?>&Destination=<?php echo trim($data["DestinationStationCode"]); ?>&DepartureDate=<?php echo $data["LocalDepartureDateTime"]; ?>" class="link_to_train_route" target="_blank">Маршрут</a>

                    <?php
                    if ($data["CarServices"]) {
                        foreach ($data["CarServices"] as $CarServices) {

                            // echo $CarServices . '</br>';
                        }
                    }
                    ?>

                </div>
                <div class="train_cell_4">



                    <?php
                    if ($data["CarGroups"]) {
                        foreach ($data["CarGroups"] as $CarType) {

                            if ($CarType["CarType"] == 'Baggage')
                                continue;
                            ?>
                            <div class="car_button">


                                <div>
                                    Тип: <?php
                                    if ($CarType["CarType"] == 'Shared') {
                                        echo 'Общий';
                                    } elseif ($CarType["CarType"] == 'Soft') {
                                        echo 'Люкс';
                                    } elseif ($CarType["CarType"] == 'Luxury') {
                                        echo 'СВ';
                                    } elseif ($CarType["CarType"] == 'Compartment') {
                                        echo 'Купе';
                                    } elseif ($CarType["CarType"] == 'ReservedSeat') {
                                        echo 'Плацкарт';
                                    } elseif ($CarType["CarType"] == 'Sedentary') {
                                        echo 'Сидячий';
                                    } elseif ($CarType["CarType"] == 'Baggage') {
                                        echo 'Багажный';
                                    } elseif ($CarType["CarType"] == 'Unknown' OR CarType["CarType"] == '') {
                                        echo 'Не определен';
                                    }
                                    ?>
                                </div>
                                <div>Мест: <?php echo $CarType["TotalPlaceQuantity"]; ?></div>

                                <?php
                                if ($to_cost) {



                                    $orig_mon = (int) $CarType["MinPrice"];



                                    $nacenka = 0;

                                    $tmp = 0;

                                    foreach ($to_cost as $key => $value) {

                                        $tmp = (int) $key;



                                        if ((int) $key > $orig_mon) {
                                            $nacenka = (int) $value;
                                            break;
                                        }
                                    }
                                }
                                ?>


                                <div>Цена от: <?php
                                $start_prices = ($orig_mon / 100) * $nacenka;

                                $start_price = round($orig_mon + $start_prices, 1);

                                echo $start_price;
                                ?> руб.
                                </div>
                                <div class="hidden">

                                    <input type="text" class="train_OriginCode" value="<?php echo $data["OriginStationCode"]; ?>" />
                                    <input type="text" class="train_DestinationCode" value="<?php echo $data["DestinationStationCode"]; ?>" />
                                    <input type="text" class="train_DepartureDate" value="<?php echo $data["LocalDepartureDateTime"]; ?>" />
                                    <input type="text" class="train_TrainNumber" value="<?php echo $data["TrainNumber"]; ?>" />
                                    <input type="text" class="train_CarType" value="<?php echo $CarType["CarType"]; ?>" />
                                    <input type="text" class="train_RoutePolicy" value="<?php echo $RoutePolicy; ?>" />

                                    <input type="text" class="LocalDepartureTime" value="<?php echo $LocalDepartureTime; ?>" />
                                    <input type="text" class="LocalDepartureDate" value="<?php echo $LocalDepartureDate; ?>" />
                                    <input type="text" class="OriginStation_name" value="<?php echo $OriginStation_name; ?>" />


                                    <input type="text" class="LocalArrivalTime" value="<?php echo $LocalArrivalTime; ?>" />
                                    <input type="text" class="LocalArrivalDate" value="<?php echo $LocalArrivalDate; ?>" />
                                    <input type="text" class="DestinationStation_name" value="<?php echo $DestinationStation_name; ?>" />

                                    <input type="text" class="train_TariffType" value="Full" />
                                    <input type="text" class="train_SpecialPlacesDemand" value="NoValue" />
                                    <input type="text" class="train_GetOnlyCarTransportationCoaches" value="false" />
                                    <input type="text" class="train_GetOnlyNonRefundableTariffs" value="false" />



                                </div>

                            </div>
                    <?php
                }
            }
            ?>



                </div>
                <div id="<?php echo $data["TrainNumber"]; ?>" class="train_cell_cars"></div>


            </div>
            <?php
        }
    } else {
        ?>

        <div class="train_error"> Не удалось получить данные!</div>


        <?php
    }


    wp_die();
}

add_action('wp_ajax_nopriv_ajax_to_trainpricing', 'ajax_to_trainpricing');
add_action('wp_ajax_ajax_to_trainpricing', 'ajax_to_trainpricing');

// получаем название станции по ее коду

function station_name($StationCode) {

    global $wpdb;

    if ($StationCode) {


        $station = $wpdb->get_results("SELECT `nameru` FROM `wp_to_nodes` WHERE `code` = " . trim($StationCode));

        $station_name = $station[0]->nameru;
    }



    return $station_name;
}

add_action('function_station_name', 'station_name');

// получаем вагоны конкретного поезда

function ajax_to_carpricing() {

    $username = get_option('to_username');
    $password = get_option('to_password');
    $host_api = get_option('to_host_api');
    $param_pos = get_option('to_param_pos');
    $authdata = base64_encode($username . ":" . $password);

    $api_method = 'Railway/V1/Search/CarPricing';
    $data = array(
        "OriginCode" => trim($_POST['train_OriginCode']),
        "DestinationCode" => trim($_POST['train_DestinationCode']),
        "DepartureDate" => trim($_POST['train_DepartureDate']),
        "TrainNumber" => trim($_POST['train_TrainNumber']),
        "CarType" => trim($_POST['train_CarType']),
        "TariffType" => trim($_POST['train_TariffType']),
        "SpecialPlacesDemand" => trim($_POST['train_SpecialPlacesDemand']),
        "GetOnlyCarTransportationCoaches" => trim($_POST['train_GetOnlyCarTransportationCoaches']),
        "GetOnlyNonRefundableTariffs" => trim($_POST['train_GetOnlyNonRefundableTariffs'])
    ); // data u want to post



    $LocalDepartureTime = trim($_POST['train_LocalDepartureTime']);
    $LocalDepartureDate = trim($_POST['train_LocalDepartureDate']);
    $OriginStation_name = trim($_POST['train_OriginStation_name']);

    $LocalArrivalTime = trim($_POST['train_LocalArrivalTime']);
    $LocalArrivalDate = trim($_POST['train_LocalArrivalDate']);
    $DestinationStation_name = trim($_POST['train_DestinationStation_name']);
    $RoutePolicy = trim($_POST['train_RoutePolicy']);
    $data_string = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $authdata,
        'Accept: application/json',
        'Content-Type: application/json',
        'cache-control: no-cache',
        'Content-Encoding: gzip',
        'pos: ' . $param_pos
            )
    );

    $return = '';

    if (curl_exec($ch) === false) {
        $return = 'Curl error: ' . curl_error($ch);
    }
    $errors = curl_error($ch);
    $result = curl_exec($ch);
    $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $out = json_decode($result, true);

    if ((int) $out["Code"] > 1) {
        ?>

        <div class="error_message" > <?php echo $out["Message"]; ?> </div>

        <?php
    } elseif ($out) {




        $to_cost = get_option('to_cost');

        foreach ($out["Cars"] as $a) {
            if ($a["CarType"] == 'Baggage')
                continue;
            if (!in_array($a["CarNumber"], $arr1)) {
                $arr1[] = $a["CarNumber"];
            }
        }

        $arr1 = array_reverse($arr1);
        foreach ($arr1 as $cars) {

            $arrIt = new RecursiveIteratorIterator(new RecursiveArrayIterator($out));

            $outputArray = array();

            foreach ($arrIt as $sub) {
                $subArray = $arrIt->getSubIterator();
                if ($subArray['CarNumber'] === $cars) {
                    $outputArray[] = iterator_to_array($subArray);
                }
            }



            if ($to_cost) {





                $price_orig = (int) $outputArray[0]["MinPrice"];


                $nacenka = 0;

                $tmp = 0;

                foreach ($to_cost as $key => $value) {

                    $tmp = (int) $key;



                    if ((int) $key > $price_orig) {
                        $nacenka = (int) $value;
                        break;
                    }
                }
            }

            $up_prices = ($price_orig / 100) * $nacenka;
            $up_prices = round($price_orig + $up_prices, 1);
            ?>


            <div class="train_cell">
                <div class="train_cell_1">
                    <div class="car_number">Вагон №: <?php echo $cars; ?>
                        <div class="hidden">


                            <input type="text" class="min_price_car" value="<?php echo $up_prices; ?>" />

                            <input type="text" class="test_price_car" value="<?php echo $price_orig; ?>" />

                            <input type="text" class="car_html_id" value="<?php echo $outputArray[0]["TrainNumber"]; ?>_<?php echo $cars; ?>" />

                            <input type="text" class="car_html_id_order" value="<?php echo $outputArray[0]["TrainNumber"]; ?>_<?php echo $cars; ?>_order" />

                            <input type="text" class="service_prov_input" value='<?php echo $outputArray[0]["Carrier"]; ?>' />
                            <input type="text" class="CarType" value='<?php echo $outputArray[0]["CarType"]; ?>' />
                            <input type="text" class="RoutePolicy" value="<?php echo $RoutePolicy; ?>" />


                            <input type="text" class="LocalDepartureTime" value="<?php echo $LocalDepartureTime; ?>" />
                            <input type="text" class="LocalDepartureDate" value="<?php echo $LocalDepartureDate; ?>" />
                            <input type="text" class="OriginStation_name" value="<?php echo $OriginStation_name; ?>" />


                            <input type="text" class="LocalArrivalTime" value="<?php echo $LocalArrivalTime; ?>" />
                            <input type="text" class="LocalArrivalDate" value="<?php echo $LocalArrivalDate; ?>" />
                            <input type="text" class="DestinationStation_name" value="<?php echo $DestinationStation_name; ?>" />





                            <input type="text" class="ServiceClass" value='<?php echo $outputArray[0]["ServiceClass"]; ?>' />
                            <input type="text" class="car_FreePlaces" value='<?php echo base64_encode(serialize($outputArray)); ?>' />

                        </div>
                    </div>
            <?php
            if ($outputArray[0]["HasElectronicRegistration"] == null || $outputArray[0]["HasElectronicRegistration"] == '') {
                echo '<div>«Внимание: Необходимо получить билет в кассе на территории России»</div>';
            }
            ?>


                </div>
                <div class="train_cell_2">

                    <div class="service_prov" ><?php echo $outputArray[0]["Carrier"]; ?></div>

                    <div class="service_class" >Класс обслуживания: <?php echo $outputArray[0]["ServiceClass"]; ?>
                        <div class="service_class_info"><?php echo $outputArray[0]["ServiceClassTranscript"]; ?></div>

                    </div>

                    <div class="category_class" >Категория: <?php echo $outputArray[0]["CarDescription"]; ?>
                        <div class="category_class_info">
                            <p><strong>Категории вагона:</strong></p>
                            <p><strong>У(N)</strong> - вагон с дополнительными услугами, где <strong>N</strong> - количество рационов питаний; </p>
                            <p><strong>Б</strong> - вагон бизнес-класса;</p>
                            <p><strong>Э</strong> - вагон эконом-класса;</p>
                            <p><strong>Ф</strong> - фирменный вагон;</p>
                            <p><strong>НФ</strong> - нефирменный вагон фирменного поезда;</p>
                            <p><strong>Ж</strong> - в вагоне возможна перевозка домашних животных.</p>
                            <p><br /> <span><strong>Признаки мест в вагоне:</strong></span></p>
                            <p><strong>БН</strong> - без указания конкретного места в вагоне;</p>
                            <p><strong>И</strong> - места для инвалидов;</p>
                            <p><strong>МЖ</strong> - продажа купе с мужскими/женскими местами.</p></div>

                    </div>


            <?php
            if ($outputArray[0]["HasDynamicPricing"] == true) {
                echo '<div>Вагон с динамическим ценообразованием</div>';
            }
            ?>


                    <?php
                    if ($outputArray[0]["Services"]) {
                        foreach ($outputArray[0]["Services"] as $CarServices) {

                            if ($CarServices == 'Bedclothes') {
                                echo '<div class="service_item">Постельное белье</div>';
                            }
                            if ($CarServices == 'Meal') {
                                echo '<div class="service_item">Питание</div>';
                            }
                            if ($CarServices == 'Wifi') {
                                echo '<div class="service_item">Wifi</div>';
                            }
                            if ($CarServices == 'Tv') {
                                echo '<div class="service_item">Телевизор</div>';
                            }
                            if ($CarServices == 'HygienicKit') {
                                echo '<div class="service_item">Санитарно-гигиенический набор</div>';
                            }
                            if ($CarServices == 'Press') {
                                echo '<div class="service_item">Пресса</div>';
                            }
                            if ($CarServices == 'AirConditioning') {
                                echo '<div class="service_item">Кондиционер</div>';
                            }
                            if ($CarServices == 'BioToilet') {
                                echo '<div class="service_item">Биотуалет</div>';
                            }
                            if ($CarServices == 'Plaid') {
                                echo '<div class="service_item">Плед</div>';
                            }
                            if ($CarServices == 'WashbasinInCompartment') {
                                echo '<div class="service_item">Умывальник</div>';
                            }
                            if ($CarServices == 'ShowerRoomInCompartment') {
                                echo '<div class="service_item">Душевая</div>';
                            }
                            if ($CarServices == 'HygienicShower') {
                                echo '<div class="service_item">Гигиенический душ</div>';
                            }
                            if ($CarServices == 'Socket220V') {
                                echo '<div class="service_item">Розетка 220В</div>';
                            }
                            if ($CarServices == 'Slippers') {
                                echo '<div class="service_item">Тапочки</div>';
                            }
                            if ($CarServices == 'PetsCarriage') {
                                echo '<div class="service_item">Провоз домашних животных</div>';
                            }
                            if ($CarServices == 'PlacesForPassengerWithBaby') {
                                echo '<div class="service_item">Места для пассажиров с детьми</div>';
                            }
                            if ($CarServices == 'Transfer') {
                                echo '<div class="service_item">Трансфер</div>';
                            }
                        }
                    }
                    ?>

                </div>

                <div class="train_cell_4">
                    <?php
                    foreach ($outputArray as $data) {
                        if ($data["MinPrice"] == 0)
                            continue;
                        if ($data["CarPlaceType"] == 'NoValue')
                            continue;
                        ?>


                        <div class="car_seat">
                            <div>
                        <?php
                        if ($data["CarPlaceType"] == 'SeparateCompartment') {
                            echo 'Отдельный отсек';
                        }
                        if ($data["CarPlaceType"] == 'SideLowerNearRestroom') {
                            echo 'Боковое нижнее у туалета';
                        }
                        if ($data["CarPlaceType"] == 'SideUpperNearRestroom') {
                            echo 'Боковое верхнее у туалета';
                        }
                        if ($data["CarPlaceType"] == 'ThirdShelf') {
                            echo 'Третья полка';
                        }
                        if ($data["CarPlaceType"] == 'Foldable') {
                            echo 'Откидное или неудобное';
                        }
                        if ($data["CarPlaceType"] == 'Upper') {
                            echo 'Верхнее';
                        }
                        if ($data["CarPlaceType"] == 'Lower') {
                            echo 'Нижнее';
                        }
                        if ($data["CarPlaceType"] == 'Usual') {
                            echo 'Обычное не у стола';
                        }
                        if ($data["CarPlaceType"] == 'LastKupeLower') {
                            echo 'Последнее купе нижнее';
                        }
                        if ($data["CarPlaceType"] == 'LastKupeUpper') {
                            echo 'Последнее купе верхнее';
                        }
                        if ($data["CarPlaceType"] == 'Middle') {
                            echo 'Среднее';
                        }
                        if ($data["CarPlaceType"] == 'WithBicycle') {
                            echo 'С велосипедом';
                        }
                        if ($data["CarPlaceType"] == 'SideLower') {
                            echo 'Боковое нижнее';
                        }
                        if ($data["CarPlaceType"] == 'SideUpper') {
                            echo 'Боковое верхнее';
                        }
                        if ($data["CarPlaceType"] == 'NearPlayground') {
                            echo 'Рядом с детской площадкой';
                        }
                        if ($data["CarPlaceType"] == 'NearTablePlayground') {
                            echo 'У стола рядом с детской площадкой';
                        }
                        if ($data["CarPlaceType"] == 'NearTable') {
                            echo 'У стола';
                        }
                        if ($data["CarPlaceType"] == 'WithPets') {
                            echo 'С животными';
                        }
                        if ($data["CarPlaceType"] == 'MotherAndBaby') {
                            echo 'Место матери и ребенка';
                        }
                        if ($data["CarPlaceType"] == 'WithChild') {
                            echo 'Для пассажиров с детьми';
                        }
                        if ($data["CarPlaceType"] == 'NearPassengersWithPets') {
                            echo 'Рядом с местами для пассажиров с животными';
                        }
                        if ($data["CarPlaceType"] == 'Invalids') {
                            echo 'Для инвалидов';
                        }
                        if ($data["CarPlaceType"] == 'InvalidsLower') {
                            echo 'Для инвалидов';
                        }
                        if ($data["CarPlaceType"] == 'InvalidsUpper') {
                            echo 'Для инвалидов';
                        }
                        if ($data["CarPlaceType"] == 'Negotiations') {
                            echo 'Переговорная';
                        }
                        if ($data["CarPlaceType"] == 'NearTableForward') {
                            echo 'У стола, по ходу';
                        }
                        if ($data["CarPlaceType"] == 'NearTableBackward') {
                            echo 'У стола, против хода';
                        }
                        if ($data["CarPlaceType"] == 'NoTableForward') {
                            echo 'Не у стола, по ходу';
                        }
                        if ($data["CarPlaceType"] == 'NoTableBackward') {
                            echo 'Не у стола, против хода';
                        }
                        if ($data["CarPlaceType"] == 'NoWindowForward') {
                            echo 'Без окна, по ходу';
                        }
                        if ($data["CarPlaceType"] == 'NoWindowBackward') {
                            echo 'Без окна, против хода';
                        }
                        if ($data["CarPlaceType"] == 'SingleForward') {
                            echo 'Одиночное, по ходу';
                        }
                        if ($data["CarPlaceType"] == 'NearRestroom') {
                            echo 'У туалета';
                        }
                        if ($data["CarPlaceType"] == 'FoldableNearRestroom') {
                            echo 'Откидное у туалета';
                        }
                        if ($data["CarPlaceType"] == 'NearRestroomAndBackward') {
                            echo 'У туалета против хода';
                        }
                        if ($data["CarPlaceType"] == 'NearRestroomAndForward') {
                            echo 'У туалета по ходу';
                        }
                        if ($data["CarPlaceType"] == 'NoTableAndNoWindow') {
                            echo 'Не у стола, без окна';
                        }
                        ?>
                            </div>

                            <div>
                                Мест: <?php echo $data["PlaceQuantity"]; ?>
                            </div>


                            <div>
                                Цена от: <?php
                if ($to_cost) {





                    $price_orig = $data["MinPrice"];


                    $nacenka = 0;

                    $tmp = 0;

                    foreach ($to_cost as $key => $value) {

                        $tmp = (int) $key;



                        if ((int) $key > $price_orig) {
                            $nacenka = (int) $value;
                            break;
                        }
                    }
                }

                $up_prices = ($price_orig / 100) * $nacenka;
                $start_price = round($price_orig + $up_prices, 1);




                echo $start_price;
                        ?> руб.

                            </div>

                        </div>

                            <?php }
                            ?>
                </div>
                <div id="<?php echo $outputArray[0]["TrainNumber"]; ?>_<?php echo $cars; ?>" class="train_cell_cars_out"></div>
                <div id="<?php echo $outputArray[0]["TrainNumber"]; ?>_<?php echo $cars; ?>_order" class="buton_order_div"></div>

            </div>
            <?php
        }
    } else {
        ?>

        <div class="train_error"> Не удалось получить данные!</div>

        <?php
    }
    wp_die();
}

add_action('wp_ajax_nopriv_ajax_to_carpricing', 'ajax_to_carpricing');
add_action('wp_ajax_ajax_to_carpricing', 'ajax_to_carpricing');

///  вывод схемы вагона / формы выбора мест
function ajax_to_carseat() {

    $CarType = trim($_POST['CarType']);

    $ServiceClass = trim($_POST['ServiceClass']);
    $car_FreePlaces = $_POST['car_FreePlaces'];
    $car_html_id = $_POST['car_html_id'];
    $arr_seat = json_decode($_POST['arr_seat']);
    $service_prov_input = $_POST['service_prov_input'];
    $LocalDepartureTime = trim($_POST['train_LocalDepartureTime']);
    $LocalDepartureDate = trim($_POST['train_LocalDepartureDate']);
    $OriginStation_name = trim($_POST['train_OriginStation_name']);

    $RoutePolicy = trim($_POST['RoutePolicy']);

    $min_price_car = trim($_POST['min_price_car']);
    $test_price_car = trim($_POST['test_price_car']);




    $LocalArrivalTime = trim($_POST['train_LocalArrivalTime']);
    $LocalArrivalDate = trim($_POST['train_LocalArrivalDate']);
    $DestinationStation_name = trim($_POST['train_DestinationStation_name']);

    $car_arr = unserialize(base64_decode($car_FreePlaces));

    $arr = array();
//var_dump($car_arr);
    foreach ($car_arr as $car) {

        $arr[] = $car ["FreePlaces"];
    }


    $comma_separated = preg_replace('/[^0-9 ,]/', '', implode(",", $arr));


    $seat = explode(",", $comma_separated);



    $to_cost = get_option('to_cost');
    $to_link_to_order = get_option('to_link_to_order');
    ?>
    <div class="train_cell_cars_in">

        <div class="hidden">

            <input type="text" class="CarType"  id="CarType" value='<?php echo $CarType; ?>' />
            <input type="text" class="ServiceClass" id="ServiceClass" value='<?php echo $ServiceClass; ?>' />
            <input type="text" class="service_prov_input" id="service_prov_input" value='<?php echo $service_prov_input; ?>' />
            <input type="text" id="LocalDepartureTime" value="<?php echo $LocalDepartureTime; ?>" />
            <input type="text" id="LocalDepartureDate" value="<?php echo $LocalDepartureDate; ?>" />
            <input type="text" id="OriginStation_name" value="<?php echo $OriginStation_name; ?>" />
            <input type="text" id="RoutePolicy" value="<?php echo $RoutePolicy; ?>" />

            <input type="text" id="LocalArrivalTime" value="<?php echo $LocalArrivalTime; ?>" />
            <input type="text" id="LocalArrivalDate" value="<?php echo $LocalArrivalDate; ?>" />
            <input type="text" id="DestinationStation_name" value="<?php echo $DestinationStation_name; ?>" />
            <input type="text" id="min_price_car" value="<?php echo $min_price_car; ?>" />
            <input type="text" id="test_price_car" value="<?php echo $test_price_car; ?>" />
        </div>

        <!--    /// тут ссылки переключения между выбором по схеме и выборам по параметрам селектами -->
        <div class="tabs_change_sits_parameters">
            <h3>Выберите желательный диапазон мест</h3>
            <ul>
                <li class="link" id="params">Выбор по параметрам</li>
                <li class="link active" id="sits">Выбор на схеме</li>
            </ul>
        </div>

    <?php ?>

    <?php
    require_once plugin_dir_path(__FILE__) . 'cars/9.php';

    // временно отключена логика выбора схем, потом включить!!!

    if ($ServiceClass == '2Э' || $ServiceClass == '2К' || $ServiceClass == '2У' || $ServiceClass == '2Т' || $ServiceClass == '2Л') {
        //require_once plugin_dir_path(__FILE__) . 'cars/9.php';
    }

    if ($ServiceClass == '3Э' || $ServiceClass == '3Д' || $ServiceClass == '3У' || $ServiceClass == '3Т' || $ServiceClass == '3Л' || $ServiceClass == '3П') {
        // require_once plugin_dir_path(__FILE__) . 'cars/1.php';
    }


    if ($ServiceClass == '1Х' || $ServiceClass == '1Ф' || $ServiceClass == '1Б' || $ServiceClass == '1Э' || $ServiceClass == '1У' || $ServiceClass == '1Т' || $ServiceClass == '1Л' || $ServiceClass == '1Д') {
        //require_once plugin_dir_path(__FILE__) . 'cars/6.php';
    }

    if ($ServiceClass == '1Х' || $ServiceClass == '1М' || $ServiceClass == '1И' || $ServiceClass == '1А') {
        //// require_once plugin_dir_path(__FILE__) . 'cars/2.php';
    }
    ?>


    </div>
        <?php
        wp_die();
    }

    add_action('wp_ajax_nopriv_ajax_to_carseat', 'ajax_to_carseat');
    add_action('wp_ajax_ajax_to_carseat', 'ajax_to_carseat');

///  выбор мест
    function ajax_to_seat() {








        $Origin_name = trim($_POST['Origin_name']);
        $Origin_id = trim($_POST['Origin_id']);
        $Destination_name = trim($_POST['Destination_name']);
        $Destination_id = trim($_POST['Destination_id']);
        $DepartureDateDirect = trim($_POST['DepartureDateDirect']);
        $DepartureDateReturn = trim($_POST['DepartureDateReturn']);



        $Type_coupe = $_POST['Type_coupe'];
        $Jarus = $_POST['Jarus'];
        $Type_seat = $_POST['Type_seat'];

        $LocalDepartureTime = trim($_POST['train_LocalDepartureTime']);
        $LocalDepartureDate = trim($_POST['train_LocalDepartureDate']);
        $OriginStation_name = trim($_POST['train_OriginStation_name']);

        $LocalArrivalTime = trim($_POST['train_LocalArrivalTime']);
        $LocalArrivalDate = trim($_POST['train_LocalArrivalDate']);
        $DestinationStation_name = trim($_POST['train_DestinationStation_name']);
        $RoutePolicy = trim($_POST['RoutePolicy']);



        $car_html_id = trim($_POST['button_id']);

        $car_html_id_ar = explode('_', $car_html_id);

        $train_number = trim($car_html_id_ar[0]);

        $car_number = trim($car_html_id_ar[1]);
        $car_seat = array(json_decode(stripslashes($_POST['car_seat'])));

        $only_direct = $_POST['only_direct'];

        $count_adult = $_POST['count_adult'];
        $count_children = $_POST['count_children'];
        $count_baby = $_POST['count_baby'];

        $all_price_car = $_POST['all_price_car'];

        $all_test_price_car = $_POST['all_test_price_car'];






        $service_prov = trim($_POST['service_prov_input']);
        $CarType = trim($_POST['CarType']);

        $ServiceClass = trim($_POST['ServiceClass']);

        $to_cost = get_option('to_cost');
        $to_link_to_order = get_option('to_link_to_order');
        ?>

    <?php
    ?>

    <?php
    $order = array();


    $order['car_seat'] = $car_seat;
    $order['count_adult'] = $count_adult;
    $order['count_children'] = $count_children;
    $order['count_baby'] = $count_baby;
    $order['Origin_name'] = $Origin_name;
    $order['Origin_id'] = $Origin_id;
    $order['Destination_name'] = $Destination_name;
    $order['Destination_id'] = $Destination_id;
    $order['DepartureDateDirect'] = $DepartureDateDirect;
    $order['DepartureDateReturn'] = $DepartureDateReturn;
    $order['train_number'] = $train_number;
    $order['car_number'] = $car_number;

    $order['service_prov'] = $service_prov;
    $order['CarType'] = $CarType;
    $order['RoutePolicy'] = $RoutePolicy;
    $order['all_price_car'] = $all_price_car;

    $order['all_test_price_car'] = $all_test_price_car;




    $order['LocalDepartureTime'] = $LocalDepartureTime;
    $order['LocalDepartureDate'] = $LocalDepartureDate;
    $order['OriginStation_name'] = $OriginStation_name;

    $order['LocalArrivalTime'] = $LocalArrivalTime;
    $order['LocalArrivalDate'] = $LocalArrivalDate;
    $order['DestinationStation_name'] = $DestinationStation_name;



    $order['ServiceClass'] = $ServiceClass;


    $order['Type_coupe'] = $Type_coupe;
    $order['Jarus'] = $Jarus;
    $order['Type_seat'] = $Type_seat;




    if (isset($_POST['car_seat_direct'])) {

        $order['car_seat_direct'] = $_POST['car_seat_direct'];
    } else {
        $order['car_seat_direct'] = false;
    }



    $order_data = base64_encode(serialize($order));
    if ($only_direct == 1) {
        ?>
        <a id="back_order_buton1" href="<?php echo $to_link_to_order; ?>?order=<?php echo $order_data; ?>" class="buton_order">Перейти к вводу данных пассажира</a>
    <?php } else {
        ?>
        <a id="back_order_buton2"  data-direct_seat="<?php echo $order_data; ?>" class="buton_order">Перейти к выбору поезда для обратного направления</a>
        <?php
    }


    wp_die();
}

add_action('wp_ajax_nopriv_ajax_to_seat', 'ajax_to_seat');
add_action('wp_ajax_ajax_to_seat', 'ajax_to_seat');
