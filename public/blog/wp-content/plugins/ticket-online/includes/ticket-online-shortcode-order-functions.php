<?php
/*
 * This file is part of is free software.
 */
/*
  Created on : 11.04.2019, 13:08:11
  Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */



if (!defined('WPINC')) {
    die;
}
### Function: add shortcode вывода формы оформления заказа

function to_funcion_order() {
    $out_html = '';
    $out_html .= '<div class="to_content">';

    if (isset($_GET['order'])) {



        $order_data = unserialize(base64_decode($_GET['order']));

        $full_price = '';

        $origin_price = '';

        $service_payment = '';



        if ($order_data['car_seat_direct'] == true) {



            $direct_order_data = unserialize(base64_decode($order_data['car_seat_direct']));

            $back_order_data = $order_data;

            $full_price = (int) $direct_order_data["all_price_car"] + (int) $back_order_data["all_price_car"];

            $origin_price = (int) $direct_order_data["all_test_price_car"] + (int) $back_order_data["all_test_price_car"];

            $service_payment = $full_price - $origin_price;

            $RoutePolicy = $direct_order_data["RoutePolicy"];
        } else {

            $direct_order_data = $order_data;

            $back_order_data = false;

            $full_price = (int) $direct_order_data["all_price_car"];

            $origin_price = (int) $direct_order_data["all_test_price_car"];

            $service_payment = $full_price - $origin_price;
            $RoutePolicy = $direct_order_data["RoutePolicy"];
        }

        //var_dump($back_order_data);
//   $RoutePolicy   -  определение международных перевозок:
//Internal - Внутреннее направление, включая страны СНГ ;
// Finland - Финляндия ;
// International Международное направление (кроме Финляндии);


        $count_adult = $direct_order_data['count_adult'];
        $count_children = (int) $direct_order_data['count_children'];
        $count_baby = (int) $direct_order_data['count_baby'];

        $car_seat = $direct_order_data['car_seat'][0];

        if (count($car_seat) > 1) {
            sort($car_seat);
            $car_seat_start = (int) $car_seat[0];
            $car_seat_end = (int) $car_seat[1];
        } else {

            $car_seat_start = (int) $car_seat[0];
            $car_seat_end = (int) $car_seat[0];
        }






        $all_pass = $count_adult + $count_children;
        $all_pass_with_baby = $count_adult + $count_children + $count_baby;

        $to_link_to_rule_page = get_option('to_link_to_rule_page');

        $countries = get_country();
        if ($countries) {
            $countries_html = '';
            $countries_html .= '<option value=""></option>
                    <option value="RU">Россия</option>
                    <option value="BY">Беларусь</option>
                    <option value="UA">Украина</option>';
            foreach ($countries as $country) {

                if ($country->alpha2code && $country->nameru && $country->alpha2code != 'BY' && $country->alpha2code != 'RU' && $country->alpha2code != 'UA') {
                    $countries_html .= ' <option value="' . $country->alpha2code . '">' . $country->nameru . '</option>';
                }
            }
        }


        $doc_type = '     <option value=""></option>
                      <option value="0">Российский паспорт</option>
                      <option value="1">Свидетельство о рождении</option>
                      <option value="2">Заграничный паспорт РФ</option>
                      <option value="3">Иностранный документ</option>
                      <option value="4">Паспорт моряка</option>
                      <option value="5">Военный билет</option>';


        //$out_html .= var_dump($order_data);
        $out_html .= '

        <div class="form_order" id="form_order_form_1">
          <form  action="" name="" id="registration_tickets">


            <div class="ticket_section ticket_section1 enebled">
              <div class="form_order_title_box">
                  <h3 class="form_order_title">Пассажир №1</h3>
                  <div class="opt_container">
                     <span class="clear_data_order clear_data_order1">Очистить данные</span>
                  </div>
              </div>
              <hr>
              <div class="passanger_data">
                <label for="name"><span>Имя <span>*</span></span><input type="text" placeholder="Введите имя" name="name" id="name1" data-validation="" class="reqired"></label>
                <label for="lastname"><span>Фамилия <span>*</span></span><input type="text" placeholder="Введите фамилию" name="lastname" id="lastname1" data-validation="" class="reqired"></label>
                <label for="fathername"><span>Отчество <span>*</span></span><input type="text" placeholder="Введите отчество" name="fathername1" id="fathername1" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Пол <span>*</span></span>
                  <select class="select_sex reqired" id="select_sex1" data-validation="">
                      <option value=""></option>
                      <option value="0">Мужской</option>
                      <option value="1">Женский</option>
                  </select>
                </div>
                <label for="birdthday"><span>Дата рождения <span>*</span></span><input type="text" placeholder="дд.мм.гггг" name="birdthday" id="birdthday1" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Тип документа <span>*</span></span>
                  <select class="document_type reqired" id="document_type1" data-validation="">';
        $out_html .= $doc_type;
        $out_html .= '</select>
                </div>
                <label for="docnumber"><span>Номер документа <span>*</span></span><input type="text" placeholder="" name="docnumber" id="docnumber1" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="wg-form__label">Страна выдачи документа <span>*</span></span>
                  <select class="select_country reqired" id="select_country1" data-validation="">';

        $out_html .= $countries_html;
        $out_html .= ' </select>
                </div>
                <hr>
                <div class="select_label">
                  <span class="select_label_text">Тариф <span>*</span></span>
                  <select class="select_tariff reqired" id="select_tariff1" data-validation="">
                    <option value="1">Полный</option>
                    <option value="2">Детский</option>
                    <option value="3">Детский без места</option>
                    <option value="14">Скидка для школьников</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="ticket_section ticket_section2';
        if ($all_pass > 1) {
            $out_html .= 'enebled';
        }
        $out_html .= '">
              <div class="form_order_title_box">
                  <h3 class="form_order_title">Пассажир №2</h3>
               <div class="opt_container">
                     <span class="clear_data_order clear_data_order2">Очистить данные</span>
                  </div>
              </div>
              <hr>
              <div class="passanger_data">
                <label for="name2"><span>Имя <span>*</span></span><input type="text" placeholder="Введите имя" name="name2" id="name2" data-validation="" class="reqired"></label>
                <label for="lastname2"><span>Фамилия <span>*</span></span><input type="text" placeholder="Введите фамилию" name="lastname2" id="lastname2" data-validation="" class="reqired"></label>
                <label for="fathername2"><span>Отчество <span>*</span></span><input type="text" placeholder="Введите отчество" name="fathername2" id="fathername2" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Пол <span>*</span></span>
                  <select class="select_sex reqired" id="select_sex2" data-validation="">
                      <option value=""></option>
                      <option value="0">Мужской</option>
                      <option value="1">Женский</option>
                  </select>
                </div>
                <label for="birdthday2"><span>Дата рождения <span>*</span></span><input type="text" placeholder="дд.мм.гггг" name="birdthday2" id="birdthday2" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Тип документа <span>*</span></span>
                  <select class="document_type2 reqired" id="document_type2" data-validation="">';
        $out_html .= $doc_type;
        $out_html .= '</select>
                </div>
                <label for="docnumber2"><span>Номер документа <span>*</span></span><input type="text" placeholder="" name="docnumber2" id="docnumber2" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="wg-form__label">Страна выдачи документа <span>*</span></span>
                  <select class="select_country2 reqired" id="select_country2" data-validation="">';

        $out_html .= $countries_html;
        $out_html .= ' </select>
                </div>
                <hr>
                <div class="select_label">
                  <span class="select_label_text">Тариф <span>*</span></span>
                  <select class="select_tariff2 reqired" id="select_tariff2" data-validation="">
                    <option value="1">Полный</option>
                    <option value="2">Детский</option>
                    <option value="3">Детский без места</option>
                    <option value="14">Скидка для школьников</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="ticket_section ticket_section3';
        if ($all_pass > 2) {
            $out_html .= 'enebled';
        }
        $out_html .= '">
              <div class="form_order_title_box">
                  <h3 class="form_order_title">Пассажир №3</h3>
                    <div class="opt_container">
                     <span class="clear_data_order clear_data_order3">Очистить данные</span>
                  </div>
              </div>
              <hr>
              <div class="passanger_data">
                <label for="name3"><span>Имя <span>*</span></span><input type="text" placeholder="Введите имя" name="name3" id="name3" data-validation="" class="reqired"></label>
                <label for="lastname3"><span>Фамилия <span>*</span></span><input type="text" placeholder="Введите фамилию" name="lastname3" id="lastname3" data-validation="" class="reqired"></label>
                <label for="fathername3"><span>Отчество <span>*</span></span><input type="text" placeholder="Введите отчество" name="fathername3" id="fathername3" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Пол <span>*</span></span>
                  <select class="select_sex3 reqired" id="select_sex3" data-validation="">
                      <option value=""></option>
                      <option value="0">Мужской</option>
                      <option value="1">Женский</option>
                  </select>
                </div>
                <label for="birdthday3"><span>Дата рождения <span>*</span></span><input type="text" placeholder="дд.мм.гггг" name="birdthday3" id="birdthday3" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Тип документа <span>*</span></span>
                  <select class="document_type3 reqired" id="document_type3" data-validation="">';
        $out_html .= $doc_type;
        $out_html .= '</select>
                </div>
                <label for="docnumber3"><span>Номер документа <span>*</span></span><input type="text" placeholder="" name="docnumber3" id="docnumber3" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="wg-form__label">Страна выдачи документа <span>*</span></span>
                  <select class="select_country3 reqired" id="select_country3" data-validation="" class="">';

        $out_html .= $countries_html;
        $out_html .= ' </select>
                </div>
                <hr>
                <div class="select_label">
                  <span class="select_label_text">Тариф <span>*</span></span>
                  <select class="select_tariff3 reqired" id="select_tariff3" data-validation="">
                    <option value="1">Полный</option>
                    <option value="2">Детский</option>
                    <option value="3">Детский без места</option>
                    <option value="14">Скидка для школьников</option>
                  </select>
                </div>
              </div>
            </div>
             <div class="ticket_section ticket_section4';
        if ($all_pass > 3) {
            $out_html .= 'enebled';
        }
        $out_html .= '">
              <div class="form_order_title_box">
                  <h3 class="form_order_title">Пассажир №4</h3>
                    <div class="opt_container">
                     <span class="clear_data_order clear_data_order4">Очистить данные</span>
                  </div>
              </div>
              <hr>
              <div class="passanger_data">
                <label for="name4"><span>Имя <span>*</span></span><input type="text" placeholder="Введите имя" name="name4" id="name4" data-validation="" class="reqired"></label>
                <label for="lastname4"><span>Фамилия <span>*</span></span><input type="text" placeholder="Введите фамилию" name="lastname4" id="lastname4" data-validation="" class="reqired"></label>
                <label for="fathername4"><span>Отчество <span>*</span></span><input type="text" placeholder="Введите отчество" name="fathername4" id="fathername4" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Пол <span>*</span></span>
                  <select class="select_sex4 reqired" id="select_sex4" data-validation="" class="">
                      <option value=""></option>
                      <option value="0">Мужской</option>
                      <option value="1">Женский</option>
                  </select>
                </div>
                <label for="birdthday4"><span>Дата рождения <span>*</span></span><input type="text" placeholder="дд.мм.гггг" name="birdthday4" id="birdthday4" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="select_label_text">Тип документа <span>*</span></span>
                  <select class="document_type4 reqired" id="document_type4" data-validation="" class="">';
        $out_html .= $doc_type;
        $out_html .= '</select>
                </div>
                <label for="docnumber4"><span>Номер документа <span>*</span></span><input type="text" placeholder="" name="docnumber4" id="docnumber4" data-validation="" class="reqired"></label>
                <div class="select_label">
                  <span class="wg-form__label">Страна выдачи документа <span>*</span></span>
                  <select class="select_country4 reqired" id="select_country4" data-validation="" class="">';

        $out_html .= $countries_html;

        $out_html .= ' </select>
                </div>
                <hr>
                <div class="select_label">
                  <span class="select_label_text">Тариф <span>*</span></span>
                  <select class="select_tariff4 reqired" id="select_tariff4" data-validation="">
                    <option value="1">Полный</option>
                    <option value="2">Детский</option>
                    <option value="3">Детский без места</option>
                    <option value="14">Скидка для школьников</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="children_container"> ';



        if ($count_baby > 0 && $count_adult < 3 && $all_pass < 4) {
            $out_html .= '<div class="ticket_section ticket_section5 child_ticket enebled">';
        } else {
            $out_html .= '<div class="ticket_section  ticket_section5 child_ticket disbled">';
        }
        $out_html .= '
              
                <div class="close_ticket_section close_ticket_section1"><span>+</span></div>
                <div class="form_order_title_box">
                    <h3 class="form_order_title">Ребенок без места №1</h3>
                 <div class="opt_container">
                     <span class="clear_data_order clear_data_order5">Очистить данные</span>
                  </div>
                </div>
                <hr>
                <div class="passanger_data">
                  <label for="name5"><span>Имя <span>*</span></span><input type="text" placeholder="Введите имя" name="name5" id="name5" data-validation="" class="reqired"></label>
                  <label for="lastname5"><span>Фамилия <span>*</span></span><input type="text" placeholder="Введите фамилию" name="lastname5" id="lastname5" data-validation="" class="reqired"></label>
                  <label for="fathername5"><span>Отчество <span>*</span></span><input type="text" placeholder="Введите отчество" name="fathername5" id="fathername5" data-validation="" class="reqired"></label>
                  <div class="select_label">
                    <span class="select_label_text">Пол <span>*</span></span>
                    <select class="select_sex5 reqired" id="select_sex5" data-validation="">
                        <option value=""></option>
                        <option value="0">Мужской</option>
                        <option value="1">Женский</option>
                    </select>
                  </div>
                  <label for="birdthday5"><span>Дата рождения <span>*</span></span><input type="text" placeholder="дд.мм.гггг" name="birdthday5" id="birdthday5" data-validation="" class="reqired"></label>
                  <div class="select_label">
                    <span class="select_label_text">Тип документа <span>*</span></span>
                    <select class="document_type5 reqired" id="document_type5" data-validation="" class="">
                        <option value=""></option>
                        <option value="1">Свидетельство о рождении</option>
                        <option value="2">Заграничный паспорт РФ</option>
                        <option value="3">Иностранный документ</option>                    
                    </select>
                  </div>
                  <label for="docnumber5"><span>Номер документа <span>*</span></span><input type="text" placeholder="" name="docnumber5" id="docnumber5" data-validation="" class="reqired"></label>
                  <div class="select_label">
                    <span class="wg-form__label">Страна выдачи документа <span>*</span></span>
                    <select class="select_country5 reqired" id="select_country5" data-validation="">';

        $out_html .= $countries_html;
        $out_html .= ' </select>
                  </div>
                </div>
              </div>';
        if ($count_baby > 1 && $count_baby < 3 && $count_adult == 2 && $all_pass < 3) {
            $out_html .= '<div class="ticket_section  ticket_section6 child_ticket enebled">';
        } else {
            $out_html .= '<div class="ticket_section  ticket_section6 child_ticket disbled">';
        }
        $out_html .= '
                <div class="close_ticket_section close_ticket_section2"><span>+</span></div>
                <div class="form_order_title_box">
                    <h3 class="form_order_title">Ребенок без места №2</h3>
                     <div class="opt_container">
                     <span class="clear_data_order clear_data_order6">Очистить данные</span>
                  </div>
                </div>
                <hr>
                <div class="passanger_data">
                  <label for="name6"><span>Имя <span>*</span></span><input type="text" placeholder="Введите имя" name="name6" id="name6" data-validation="" class="reqired"></label>
                  <label for="lastname6"><span>Фамилия <span>*</span></span><input type="text" placeholder="Введите фамилию" name="lastname6" id="lastname6" data-validation="" class="reqired"></label>
                  <label for="fathername6"><span>Отчество <span>*</span></span><input type="text" placeholder="Введите отчество" name="fathername6" id="fathername6" data-validation="" class="reqired"></label>
                  <div class="select_label">
                    <span class="select_label_text">Пол <span>*</span></span>
                    <select class="select_sex6 reqired" id="select_sex6" data-validation="" >
                        <option value=""></option>
                        <option value="0">Мужской</option>
                        <option value="1">Женский</option>
                    </select>
                  </div>
                  <label for="birdthday6"><span>Дата рождения <span>*</span></span><input type="text" placeholder="дд.мм.гггг" name="birdthday6" id="birdthday6" data-validation="" class="reqired"></label>
                  <div class="select_label">
                    <span class="select_label_text">Тип документа <span>*</span></span>
                    <select class="document_type6 reqired" id="document_type6" data-validation="">
                        <option value=""></option>
                        <option value="1">Свидетельство о рождении</option>
                        <option value="2">Заграничный паспорт РФ</option>
                        <option value="3">Иностранный документ</option>
                      </select>
                  </div>
                  <label for="docnumber6"><span>Номер документа <span>*</span></span><input type="text" placeholder="" name="docnumber6" id="docnumber6" data-validation="" class="reqired"></label>
                  <div class="select_label">
                    <span class="wg-form__label">Страна выдачи документа <span>*</span></span>
                    <select class="select_country6 reqired" id="select_country6" data-validation="">';

        $out_html .= $countries_html;
        $out_html .= ' </select>
                  </div>
                </div>
              </div>
            </div>
            <div id="add_baby_btn" class="add_child_btn';
        if ($count_baby > 1 && $all_pass_with_baby > 3) {
            $out_html .= ' disaled_btn';
        }

        $out_html .= '">
              <span class="button_add">+</span>
              <span class="btn_label">Добавить ребенка <span>(до 5 лет бесплатно)</span></span>
            </div>';

        $out_html .= '
         
            <div class="ticket_section order_contacts_data enebled">
              <h3 class="order_contacts_title form_order_title">Контактные данные заказа</h3>
              <hr>
              <div class="data_container">
                <label for="order_email"><span>Электронная почта <span>*</span></span><input type="text" class="order_email reqired" id="order_email" name="order_email" placeholder="Ваш e-mail" data-validation="" ></label>
                <label for="order_phone"><span>Телефон <span>*</span></span><input type="text" class="order_phone reqired" id="order_phone" name="order_phone" placeholder="+7" data-validation="" ></label>
                <p class="d_info">На указанные данные будет отправлена информация о заказе.</p>
              </div>
            </div>
            <div class="form_information">
              <h6>Будьте внимательны при заполнении личных данных пассажиров!</h6>
              <p>Эти данные будут указаны в билете, и проводник будет проверять их при посадке в вагон. При наличии более одной ошибки билет будет недействительным, и проезд по нему будет невозможен.</p>
              <p><span>*</span> Поля, обязательные к заполнению</p>
            </div>
            <div class="agree_checkbox_container">
              <input type="checkbox" name="rools_agree" class="rools_agree" id="rools_agree" checked="checked">
              <label for="rools_agree">Я согласен на <a href="' . $to_link_to_rule_page . '" target="blank">обработку персональных данных</a></label>
            </div>
            <div class="submit_container">
           
              <button type="submit" class="confirm_order" id="create_order">Подтвердить данные</button>
            </div>
          </form>';

        $out_html .= '<div class="to_result_html_order" id="to_result_html_order"></div>';


        $out_html .= '<div class="order_aside_info">
            <div class="ord_info_title">Ваш заказ</div>';




        $out_html .= '<div class="destination_number"><span class="number">поезд № ' . $direct_order_data['train_number'] . '</span></div>
            <div class="from_to">' . $direct_order_data["Origin_name"] . ' — ' . $direct_order_data["Destination_name"] . '</div>
            <div class="train_info"><span>' . $direct_order_data['service_prov'] . '</span></div>
               
            <div class="hidden_details">
             <hr>
              <div class="first_point">' . $direct_order_data['Origin_name'] . '</div>
              <div class="first_station_name">' . $direct_order_data['OriginStation_name'] . '</div>
              <div class="departure_time"><span class="time">' . $direct_order_data['LocalDepartureTime'] . '</span> <span class="abb_point"><span>местное</span>
                  <span>' . $direct_order_data['LocalDepartureDate'] . '</span></span></div>
              <div class="dots_red">
                <span></span>
             <span></span>
             <span></span>

              </div>
                <div class="first_point">' . $direct_order_data['Destination_name'] . '</div>
              <div class="first_station_name">' . $direct_order_data['DestinationStation_name'] . '</div>
              <div class="arrival_time"><span class="time">' . $direct_order_data['LocalArrivalTime'] . '</span><span class="abb_point"><span>местное</span>
                  <span>' . $direct_order_data['LocalArrivalDate'] . '</span></span>
<hr>
<div class="section_order_info_detail">
<h3 class="info__detail_title">Количество мест: 1</h3>
<div class="order_info_place-table-wrap">
<div class="order_info_place-table">
<table>
<tbody>
<tr>
<th>Вагон</th>
<th>Места</th>
</tr>
<tr>
<td><span class="t_car_order_info">№06</span>2У</td>
<td><span class="t_place_order_info">28</span></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
              <div class="close_details">скрыть</div>
            </div>
            <div class="more_info">Подробнее</div>';

        if ($back_order_data) {

            $out_html .= '<div class="destination_number"><span class="number">поезд № ' . $back_order_data['train_number'] . '</span></div>
            <div class="from_to">' . $back_order_data["Origin_name"] . ' — ' . $back_order_data["Destination_name"] . '</div>
            <div class="train_info"><span>' . $back_order_data['service_prov'] . '</span></div>
            <div class="hidden_details2">
             <hr>
               <div class="first_point">' . $back_order_data['Origin_name'] . '</div>
              <div class="first_station_name">' . $back_order_data['OriginStation_name'] . '</div>
              <div class="departure_time"><span class="time">' . $back_order_data['LocalDepartureTime'] . '</span> <span class="abb_point"><span>местное</span>
                  <span>' . $back_order_data['LocalDepartureDate'] . '</span></span></div>
              <div class="dots_red">
                <span></span>
                <span></span>
            <span></span>
              </div>
               <div class="first_point">' . $back_order_data['Destination_name'] . '</div>
               <div class="first_station_name">' . $back_order_data['DestinationStation_name'] . '</div>
              <div class="arrival_time"><span class="time">' . $back_order_data['LocalArrivalTime'] . '</span><span class="abb_point"><span>местное</span>
                  <span>' . $back_order_data['LocalArrivalDate'] . '</span></span>
<hr>
<div class="section_order_info_detail">
<h3 class="info__detail_title">Количество мест: 1</h3>
<div class="order_info_place-table-wrap">
<div class="order_info_place-table">
<table>
<tbody>
<tr>
<th>Вагон</th>
<th>Места</th>
</tr>
<tr>
<td><span class="t_car_order_info">№06</span>2У</td>
<td><span class="t_place_order_info">28</span></td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
              <div class="close_details2">скрыть</div>
            </div>
            <div class="more_info2">Подробнее</div>';
        }


        $out_html .= '<div class="order_total"><span>Сумма заказа:</span> <span><span>от</span> ' . $full_price . ' руб.</span>
                 <div class="order_total_info"><span>info</span><div class="service_payment">
                 <span>В том числе сервисный сбор от</span> ' . $service_payment . ' руб.</span></div>
                     </div>


</div>
          </div>';


        $out_html .= '</div>';
    } else {



        $out_html .= '<div class="error_message" >Данные указаны не верно.</div>';
    }





    $out_html .= '</div>';


    return $out_html;
}

add_shortcode('to_order', 'to_funcion_order');

### Function: вывода стран

function get_country() {

    global $wpdb;

    $country = $wpdb->get_results("SELECT `countryid`, `alpha2code`, `nameru` FROM `wp_to_country` WHERE `isactive` = 1 ORDER BY `nameru` ASC");

    if ($country) {

        return $country;
    } else {
        return false;
    }

    wp_die();
}

function ajax_to_create_order() {

    $username = get_option('to_username');
    $password = get_option('to_password');
    $host_api = get_option('to_host_api');
    $param_pos = get_option('to_param_pos');
    $authdata = base64_encode($username . ":" . $password);

    $type = '$type';

    $api_method = 'Order/V1/Reservation/Create';

    $to_link_to_ticket = get_option('to_link_to_ticket');

    $type_Customers = (string) "ApiContracts.Order.V1.Reservation.OrderFullCustomerRequest, ApiContracts";
    $data_Customers = array(
        "$type" => $type_Customers,
        "DocumentNumber" => "4601123450",
        "DocumentType" => "RussianPassport",
        "DocumentValidTill" => null,
        "CitizenshipCode" => "RU",
        "BirthPlace" => null,
        "FirstName" => "Иван",
        "MiddleName" => "Иванович",
        "LastName" => "Иванов",
        "Sex" => "Male",
        "Birthday" => "1976-10-12T00:00:00",
        "Index" => 1
    );


    $data_Passengers = array(
        "Category" => "Adult",
        "PreferredAdultTariffType" => "Full",
        "RailwayBonusCards" => null,
        "IsInvalid" => false,
        "DisabledPersonId" => null,
        "TransportationRequirement" => null,
        "TransitDocument" => "NoValue",
        "Phone" => null,
        "ContactEmail" => null,
        "ContactEmailOrPhone" => null,
        "IsMarketingNewsletterAllowed" => false,
        "IsNonRefundableTariff" => false,
        "RefuseToReceiveAutomaticSpecialTariff" => false,
        "OrderCustomerIndex" => 1
    );


    $data_PlaceRange = array(
        "From" => 5,
        "To" => 38
    );


    $data_PlaceRange2 = array(
        "From" => 5,
        "To" => 38
    );

    $type_ReservationItems = (string) "ApiContracts.Railway.V1.Messages.Reservation.RailwayReservationRequest, ApiContracts";



    $data_ReservationItems = array(
        "$type" => $type_ReservationItems,
        "OriginCode" => "2000000",
        "DestinationCode" => "2060600",
        "DepartureDate" => "2019-06-26T00:00:00",
        "TrainNumber" => "070Ч",
        "CarNumber" => "06",
        "CarType" => "Compartment",
        "LowerPlaceQuantity" => 0,
        "UpperPlaceQuantity" => 0,
        "CabinGenderKind" => "Mixed",
        "CarStorey" => "NoValue",
        "PlaceRange" => $data_PlaceRange,
        "CabinPlaceDemands" => "NoValue",
        "SetElectronicRegistration" => true,
        "Bedding" => null,
        "OnRequestMeal" => false,
        "ServiceClass" => "2Э",
        "InternationalServiceClass" => null,
        "AdditionalPlaceRequirements" => "NoValue",
        "ProviderPaymentForm" => "Card",
        "GiveAdditionalTariffForChildIfPossible" => false,
        "SpecialPlacesDemand" => "NoValue",
        "Passengers" => [$data_Passengers],
        "Index" => 1,
        "AgentReferenceId" => null,
        "AgentPaymentId" => null,
        "ClientCharge" => 2000
    );

    $data_ReservationItems2 = array(
        "$type" => $type_ReservationItems,
        "OriginCode" => "2060600",
        "DestinationCode" => "2000000",
        "DepartureDate" => "2019-06-30T00:00:00",
        "TrainNumber" => "069Я",
        "CarNumber" => "06",
        "CarType" => "Compartment",
        "LowerPlaceQuantity" => 0,
        "UpperPlaceQuantity" => 0,
        "CabinGenderKind" => "NoValue",
        "CarStorey" => "NoValue",
        "PlaceRange" => $data_PlaceRange2,
        "CabinPlaceDemands" => "NoValue",
        "SetElectronicRegistration" => true,
        "Bedding" => null,
        "OnRequestMeal" => false,
        "ServiceClass" => "2Э",
        "InternationalServiceClass" => null,
        "AdditionalPlaceRequirements" => "NoValue",
        "ProviderPaymentForm" => "Card",
        "GiveAdditionalTariffForChildIfPossible" => false,
        "SpecialPlacesDemand" => "NoValue",
        "Passengers" => [$data_Passengers],
        "Index" => 2,
        "AgentReferenceId" => null,
        "AgentPaymentId" => null,
        "ClientCharge" => 2000
    );


    $data = array(
        "ContactPhone" => "+79123456789",
        "ContactEmails" => array("test@test.ru"),
        "RefuseToReceiveAutomaticRoundTripDiscountForRailwayTickets" => false,
        "Customers" => [$data_Customers],
        "ReservationItems" => [$data_ReservationItems, $data_ReservationItems2],
        "CheckDoubleBooking" => true
    ); // data u want to post




    $data_string = json_encode($data);



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $authdata,
        'Accept: application/json',
        'Content-Type: application/json',
        'cache-control: no-cache',
        'Content-Encoding: gzip',
        'pos: ' . $param_pos
            )
    );

    $return = '';

    if (curl_exec($ch) === false) {
        $return = 'Curl error: ' . curl_error($ch);
    }
    $errors = curl_error($ch);
    $result = curl_exec($ch);
    $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $out = json_decode($result, true);
    ?>

    <div class="order_container">

        <?php
        /// если вернулся код ошибки
        if ((int) $out["Code"] > 0) {
            ?>

            <div class="form_order_title_box">
                <h3 class="form_order_title"><?php echo $out["Message"]; ?></h3>
            </div>
            <hr>
            <div class="submit_container2">
                <a href="<?php echo $to_link_to_ticket; ?>" id="new_order" class="back_btn btn new_order">Выбрать другие билеты</a>

            </div>

        <?php } else { ?>

            <div class="order_container_order_info">

                <?php print_r($out); //для отладки ответа  ?>

            </div>

            <div class="submit_container2">
                <a id="change_order" class="back_btn btn">Изменить данные</a> <a id="cancel_order" class="back_btn btn" data-cancel_order="<?php echo (int)$out["OrderId"]; ?>">Отменить заказ</a>

                <h3 class="reserve_order_title">Резерв действителен 14 мин. 59 сек. </h3>
                <button type="submit" class="confirm_order" id="payment_order">Оплатить</button>
            </div>

        <?php } ?>

    </div>

    <?php
    wp_die();
}

add_action('wp_ajax_nopriv_ajax_to_create_order', 'ajax_to_create_order');
add_action('wp_ajax_ajax_to_create_order', 'ajax_to_create_order');



// функция отмены заказа( резерва без оплаты)

function ajax_to_cancel_order() {

    $username = get_option('to_username');
    $password = get_option('to_password');
    $host_api = get_option('to_host_api');
    $param_pos = get_option('to_param_pos');
    $authdata = base64_encode($username . ":" . $password);

    $type = '$type';

    $api_method = 'Order/V1/Reservation/Cancel';

    $to_link_to_ticket = get_option('to_link_to_ticket');

    $data = array(
        "OrderId" => trim($_POST['cancel_order_id']),
        "OrderItemIds" => null,
        "OrderCustomerIds" => null
    ); // data u want to post




    $data_string = json_encode($data);



    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $authdata,
        'Accept: application/json',
        'Content-Type: application/json',
        'cache-control: no-cache',
        'Content-Encoding: gzip',
        'pos: ' . $param_pos
            )
    );

    $return = '';

    if (curl_exec($ch) === false) {
        $return = 'Curl error: ' . curl_error($ch);
    }
    $errors = curl_error($ch);
    $result = curl_exec($ch);
    $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $out = json_decode($result, true);
    ?>

    <div class="order_cancel_container">

    

            <div class="form_order_title_box">
                <h3 class="form_order_title">Ваш заказ отменен</h3>
            </div>
            <hr>
            <div class="submit_container2">
                <a href="<?php echo $to_link_to_ticket; ?>" id="new_order" class="back_btn btn new_order">Выбрать другие билеты</a>

            </div>

       

    </div>

    <?php
    wp_die();
}

add_action('wp_ajax_nopriv_ajax_to_cancel_order', 'ajax_to_cancel_order');
add_action('wp_ajax_ajax_to_cancel_order', 'ajax_to_cancel_order');


