<?php

/*
 * This file is part of is free software.
 */
/*
  Created on : 09.04.2019, 12:21:40
  Author     : Dmitrij Nedeljković https://dmitrydevelopment.ru/
 */

if (!defined('WPINC')) {
    die;
}

### Function: insert/update Transport Nodes

function to_nodes($authdata, $host_api, $param_pos) {
    global $wpdb;
    $wpdb->show_errors(); // включит показ ошибок
    $table_name = $wpdb->get_blog_prefix() . 'to_nodes';
    $wpdb->query("TRUNCATE " . $table_name);
    // заполняем таблицу городов
    $api_method = 'Info/V1/References/TransportNodes';
    $data = array("LastUpdated" => null, "Type" => 'RailwayStation', "IncludeInvisible" => false); // data u want to post

    $data_string = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 320);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Basic ' . $authdata,
        'Accept: application/json',
        'Content-Type: application/json',
        'cache-control: no-cache',
        'Content-Encoding: gzip',
        'pos: ' . $param_pos
            )
    );

    $return = '';

    if (curl_exec($ch) === false) {
        $return = 'Curl error: ' . curl_error($ch);
    }
    $errors = curl_error($ch);
    $result = curl_exec($ch);
    $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $out = json_decode($result, true);

    if ($out) {
        foreach ($out["TransportNodes"] as $data) {

            if ($data["Location"]["Latitude"] && $data["Location"]["Longitude"]) {
                $location = $data["Location"]["Latitude"] . ',' . $data["Location"]["Longitude"];
            } else {
                $location = '';
            }
            $wpdb->insert(
                    $table_name, array('transportnodeid' => $data["TransportNodeId"], 'countryid' => $data["CountryId"], 'regionid' => $data["RegionId"],
                'cityid' => $data["CityId"], 'code' => $data["Code"], 'nameru' => $data["NameRu"],
                'nameen' => $data["NameEn"], 'type' => $data["Type"], 'popularityindex' => $data["PopularityIndex"], 'description' => $data["Description"],
                'isactive' => $data["IsActive"], 'isvisible' => $data["IsVisible"], 'issuburban' => $data["IsSuburban"], 'updated' => $data["Updated"],
                'utctimeoffset' => $data["UtcTimeOffset"], 'location' => $location)
            );
        }
    } else {
        $return = 'Не удалось получить данные!';
    }
    return $return;
}
