<?php

/**
 * Plugin Name: RZD Ticket online
 * Version: 1.0
 * Description: Покупка билетов на поезда РЖД онлайн 2.
 * Author: Dmitrij Nedeljković
 * Author URI: https://dmitrydevelopment.com/
 */

if (!defined('WPINC')) {
    die;
}

?>
<script>
    window.ajaxUrl = '<?php echo admin_url( 'admin-ajax.php', 'relative' ); ?>';
</script>
<?php

### Function: add CSS and js
add_action('wp_enqueue_scripts', 'ticket_online_scripts');

function ticket_online_scripts() {
    wp_enqueue_style('ticket-online', plugin_dir_url(__FILE__) . 'css/ticket-online.css', array(), '1.0.0', 'all');
    wp_enqueue_style('ticket-online-car', plugin_dir_url(__FILE__) . 'css/ticket-online-car.css', array(), '1.0.0', 'all');
    wp_enqueue_style('jquery-datetimepicker-min', plugin_dir_url(__FILE__) . 'css/jquery/jquery.datetimepicker.min.css', array(), '1.3.4', 'all');
    wp_enqueue_style('ticket-online-order', plugin_dir_url(__FILE__) . 'css/ticket-online-order.css', array(), '1.0.0', 'all');
    wp_enqueue_style('ticket-online-customer', plugin_dir_url(__FILE__) . 'css/ticket-online-customer.css', array(), '1.0.0', 'all');
    wp_enqueue_style('ticket-online-shedule', plugin_dir_url(__FILE__) . 'css/ticket-online-shedule.css', array(), '1.0.0', 'all');

    wp_enqueue_script('ticket-online', plugin_dir_url(__FILE__) . 'js/ticket-online.js', array(), '1.0.0', true);
    wp_enqueue_script('js-cookie', plugin_dir_url(__FILE__) . 'js/jquery/js.cookie.js', false);
    wp_enqueue_script('jquery-datetimepicker-full-min', plugin_dir_url(__FILE__) . 'js//jquery/jquery.datetimepicker.full.min.js', array('jquery'), '1.3.4', true);
    wp_enqueue_script('number_plugin', plugin_dir_url(__FILE__) . 'js/number_plugin.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('ticket-online-order', plugin_dir_url(__FILE__) . 'js/ticket-online-order.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('ticket-online-customer', plugin_dir_url(__FILE__) . 'js/ticket-online-customer.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('ticket-online-shedule', plugin_dir_url(__FILE__) . 'js/ticket-online-shedule.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('form-validator', plugin_dir_url(__FILE__) . 'js/jquery.form-validator.min.js', array('jquery'), '1.0.0', true);
}

### Function: Ticket online Option Menu
add_action('admin_menu', 'to_menu');

function to_menu() {
    if (function_exists('add_options_page')) {
        add_options_page('Билеты РЖД', 'Билеты РЖД', 'manage_options', 'ticket-online/ticket-online-options.php');
    }
}

### Function: activation plugin
function activation_plugin() {
    require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-activator.php';
}

register_activation_hook(__FILE__, 'activation_plugin');

### Function: deactivation plugin
function deactivation_plugin() {
    require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-deactivator.php';
}

register_deactivation_hook(__FILE__, 'deactivation_plugin');

### Function: insert/update country
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-country.php';

### Function: insert/update city
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-city.php';

### Function: insert/update Transport Nodes
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-nodes.php';

### Function: add shortcode
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-shortcode.php';

### Function: add shortcode  запроса билетов
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-shortcode-functions.php';

### Function: add shortcode вывода маршрутов
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-shortcode-route-functions.php';

### Function: add shortcode вывода формы оформления заказа
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-shortcode-order-functions.php';

### Function: add shortcode вывода содержимого личного кабинета пользователя
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-shortcode-customer.php';

### Function: add shortcode вывода базового расписания
require_once plugin_dir_path(__FILE__) . 'includes/ticket-online-shortcode-shedule-functions.php';

### Function: вывода городов в админке на сео вкладке
add_action('wp_ajax_get_city', 'get_city_callback');

function get_city_callback() {
    global $wpdb;

    if (isset($_POST['term'])) {
        $places = $wpdb->get_results("SELECT `code`,`nameru` FROM `wp_to_nodes` WHERE `nameru` LIKE '" . trim($_POST['term']) . "%' ORDER BY `popularityindex` DESC LIMIT 10");
        if ($places) {
            foreach ($places as $place) {
                ?>
                <li><a data-node_id="<?php echo $place->code; ?>" data-node_nameru="<?php echo $place->nameru; ?>"><?php echo $place->nameru; ?></a></li>
                <?php
            }
        } else {
            ?>
            <li><a data-node_id="null">По вашему запросу ничего не найдено</a></li>
            <?php
        }
    }

    wp_die();
}
