<?php

if (!defined('WPINC')) {
    die;
}

### Variables
$base_name = plugin_basename('ticket-online/ticket-online-options.php');
$base_page = 'admin.php?page=' . $base_name;
$text = '';
$username = get_option('to_username');
$password = get_option('to_password');
$host_api = get_option('to_host_api');
$param_pos = get_option('to_param_pos');
$authdata = base64_encode($username . ":" . $password);

// Options
$to_cost = get_option('to_cost');
$to_link_to_ticket = get_option('to_link_to_ticket');
$to_link_to_order = get_option('to_link_to_order');
$to_link_to_route_page = get_option('to_link_to_route_page');
$to_link_to_rule_page = get_option('to_link_to_rule_page');
$to_link_to_shedule_page = get_option('to_link_to_shedule_page');

### Form Processing
if (!empty($_POST['Submit'])) {

    if (!empty($_POST['to_host_api'])) {
        $host_api = trim($_POST['to_host_api']);
        if (substr($host_api, strlen($host_api) - 1) == "/") {
            $host_api = substr($host_api, 0, strlen($host_api) - 1);
        }
        update_option('to_host_api', $host_api);
    }

    if (!empty($_POST['to_username'])) {
        $to_username = trim($_POST['to_username']);
        update_option('to_username', $to_username);
    }

    if (!empty($_POST['to_password'])) {
        $to_password = trim($_POST['to_password']);
        update_option('to_password', $to_password);
    }

    if (!empty($_POST['to_param_pos'])) {
        $to_param_pos = trim($_POST['to_param_pos']);
        update_option('to_param_pos', $to_param_pos);
    }

    if (!empty($_POST['to_cost_max'])) {
        $a = $_POST['to_cost_max'];

        $array_filtered_a = array_filter($a, function ($v) { return $v > 0; });
        $b = $_POST['to_cost'];
        $array_filtered_b = array_filter($b, function ($v) { return $v > 0; });
        $to_cost = array_combine($array_filtered_a, $array_filtered_b);

        update_option('to_cost', $to_cost);
    }

    if (!empty($_POST['to_link_to_route_page'])) {
        $to_link_to_route_page = trim($_POST['to_link_to_route_page']);
        update_option('to_link_to_route_page', $to_link_to_route_page);
    }

    if (!empty($_POST['to_link_to_ticket'])) {
        $to_link_to_ticket = trim($_POST['to_link_to_ticket']);
        update_option('to_link_to_ticket', $to_link_to_ticket);
    }

    if (!empty($_POST['to_link_to_shedule_page'])) {
        $to_link_to_shedule_page = trim($_POST['to_link_to_shedule_page']);
        update_option('to_link_to_shedule_page', $to_link_to_shedule_page);
    }

    if (!empty($_POST['to_link_to_rule_page'])) {
        $to_link_to_rule_page = trim($_POST['to_link_to_rule_page']);
        update_option('to_link_to_rule_page', $to_link_to_rule_page);
    }

    if (!empty($_POST['to_link_to_order'])) {
        $to_link_to_order = trim($_POST['to_link_to_order']);
        update_option('to_link_to_order', $to_link_to_order);
    }

    $text = '<p style="color: green;">Настройки успешно сохранены!</p>';
}

if (!empty($_POST['Reload'])) {

    if (to_country($authdata, $host_api, $param_pos) == '') {
        $text .= '<p style="color: green;">Данные о странах успешно обновленны!</p>';
    } else {
        $text .= '<p style="color: red;">Не удалось получить данные о странах!</p>';
    }

    if (to_city($authdata, $host_api, $param_pos) == '') {
        $text .= '<p style="color: green;">Данные о городах успешно обновленны!</p>';
    } else {
        $text .= '<p style="color: red;">Не удалось получить данные о городах!</p>';
    }

    if (to_nodes($authdata, $host_api, $param_pos) == '') {
        $text .= '<p style="color: green;">Данные о станциях успешно обновленны!</p>';
    } else {
        $text .= '<p style="color: red;">Не удалось получить данные о станциях!</p>';
    }

}

$site_url = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://' . $_SERVER['SERVER_NAME'];

?>

<style>
.content_to h1{
    margin-bottom: 100px !important;
    margin-top: 50px !important;
}
.to_options_string {
    width: 30%;
    height: 40px;
}
.to_options_string span {
    top: 4px;
    position: relative;
}
.to_options_string input[type="text"],  .to_options_string input[type="number"]{
    float: right;
}
.content_to h3{
    width: 50%;
    margin-bottom: 30px !important;
}
.gen_tab_item{
    display: none;
}
.gen_tab_item.open{
    display: block;
}
.admin_tab_general{
    border-bottom: 1px solid #d7d7d7;
}
.admin_tab_general ul{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: flex-start;
    align-items: flex-start;
    justify-content: flex-start;
    margin: 0;
}
.admin_tab_general ul li{
    font-size: 16px;
    font-weight: bold;
    line-height: 1.5;
    padding: 10px 30px;
    cursor: pointer;
    border: 1px solid #d6d6d6;
    border-bottom: 0;
    margin: 0;
}
.admin_tab_general ul li.active{
    background-color: #d6d6d6;
}
.tab_content{
    padding: 20px 0;
}
.direction{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: flex-start;
    width: calc(100% + 10px);
    margin-left: -5px;
}
.seo_tickets_container{
    position: relative;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: space-between;
    border-bottom: 1px solid #ccc;
    margin-bottom: 10px;
    padding-bottom: 10px;
}
.line_number {
    padding-right: 10px;
}
.line_number span{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: center;
    width: 20px;
    height: 20px;
    background-color: #000;
    border-radius: 50%;
    color: #fff;
    font-size: 12px;
    font-weight: bold;
}
.seo_tickets_container .seo_input_box{
    width: 25%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0 5px;
    position: relative;
}
.seo_tickets_container .seo_input_box>*{
    display: block;
    width: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0 15px;
}
.seo_tickets_container .seo_input_box>input{
    min-height: 40px;
}
.seo_tickets_container .search_list{
    position: absolute;
    top: 100%;
    left: 5px;
    width: calc(100% - 10px);
    height: auto;
    background-color: #fff;
    margin: 0;
    padding: 15px 0;
    border: 1px solid #f2f2f2;
    z-index: 10;
}
.seo_tickets_container .search_list li{
    padding: 5px 15px;
    cursor: pointer;
    margin: 0;
}
.seo_tickets_container .search_list li:hover{
    background-color: #f2f2f2;
}
.seo_tickets_container .seo_input_box>.popup_condition{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: center;
    background-color: #fff;
    height: 40px;
    cursor: pointer;
}
.seo_tickets_container .seo_input_box .btn_save{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: center;
    border: 1px solid #000;
    background-color: #000;
    color: #fff;
    cursor: pointer;
    height: 40px;
    padding-bottom: 1px;
}
.seo_tickets_container .seo_input_box .btn_save:hover{
    background-color: #fff;
    color: #000;
}
.SEO_popup textarea{
    display: block;
    width: 100%;
    height: 400px;
    border: 0;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 10px 15px;
    border: 1px solid #000;
}
.btns_pop_box{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: flex-end;
    margin-top: 10px;
}
.btns_pop_box .btn_save{
    padding: 0 30px 1px;
    height: 30px;
}
.close_popup{
    font-size: 14px;
    line-height: 1;
    cursor: pointer;
    margin-bottom: 15px;
}
.close_popup:hover{
    text-decoration: underline;
}
.if_field,
.if_empty{
    display: none;
}
.SEO_popup{
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
}
.form_wrapp{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: center;
    z-index: 100;
    overflow: auto;
    width: 100%;
    height: 100%;
}
.SEO_popup form{
    position: relative;
    width: 90%;
    max-width: 780px;
    height: auto;
    z-index: 2;
    background-color: #fff;
    padding: 30px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.bg_dark{
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, .3);
    z-index: 1;
}
.seo_tickets_box_list{
    width: calc(100% - 50px);
}
.remove_list_item{
    width: 50px;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: flex-end;
    height: 100%;
}
.delete_item{
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background-color: red;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: center;
    color: #fff;
    font-weight: bold;
    cursor: pointer;
    padding-bottom: 3px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.add_item{
    position: absolute;
    bottom: -10px;
    right: 0;
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    justify-content: center;
    width: 20px;
    height: 20px;
    background-color: blue;
    color: #fff;
    border-radius: 50%;
    font-weight: bold;
    cursor: pointer;
    padding-bottom: 3px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.add_item:hover{
    background-color: #fff;
    color: blue;
}
.direction_title{
    display: block;
    width: 100%;
    font-size: 14px;
    margin-bottom: 10px;
}
.seo_item{
    margin-bottom: 10px;
}
.price_title{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -webkit-flex-wrap: wrap;
    -moz-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    -o-flex-wrap: wrap;
    flex-wrap: wrap;
    width: 30%;
}
.price_title p{
    width: calc(40% - 2px);;
}
.opt_string_wrap{
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -webkit-flex-wrap: wrap;
    -moz-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    -o-flex-wrap: wrap;
    flex-wrap: wrap;
}
.to_options_string input{
    width: calc(40% - 2px);
    margin: 0;
    margin-right: 2px;
}
.to_options_string2 input{
    width: 400px;
}
.add_rov{
    width: 20%;
}
.remove_row{
    width: 10%;
}
#cost_item{
    height: auto;
    margin-bottom: 10px;
}
</style>

<div class="content_to">
    <div class="wrap">
        <h1 class="to_form">Настройки плагина покупки билетов на поезда РЖД онлайн</h1>
        <!-- // Тут заголовки табов -->
        <!-- // Основные настройки - СЕО настройки -->
        <div class="admin_tab_general">
            <ul>
                <li class="generel_link active" id="gen">Основные настройки</li>
                <li class="generel_link " id="seo">СЕО настройки</li>
            </ul>
        </div>
        <!-- // Начало содержимого таба Основные настройки -->
        <div class="general_tabs">
            <div class="gen_tab_item open" data-link="gen">
                <table class="form-table">
                    <tr>
                        <td valign="top" width="24%" style="font-weight: 600;">Информация об остатках на счетах партнёра:</td>
                        <td valign="top" style="padding-left: 23px;">
                            <div class="to_options">
                                <?php echo to_balance($authdata, $host_api, $param_pos); ?>
                            </div>
                        </td>
                    </tr>
                </table>
                <form method="post" action="<?php echo admin_url('admin.php?page=' . plugin_basename(__FILE__)); ?>">
                    <?php
                    if (!empty($text)) {
                        echo '<div id="message" class="updated fade"><p>' . $text . '</p></div>';
                    }
                    ?>
                    <div class="wrap">
                        <table class="form-table">
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Данные для доступа к API РЖД:</td>
                                <td valign="top">
                                    <div class="to_options">

                                        <div class="to_options_string">
                                            <span>URL:</span> <input type="text" name="to_host_api" value="<?php echo $host_api; ?>" placeholder="Введите ссылку"></br>
                                        </div>
                                        <div class="to_options_string">
                                            <span>Логин:</span>  <input type="text" name="to_username" value="<?php echo $username; ?>" placeholder="Введите ссылку"></br>
                                        </div>
                                        <div class="to_options_string">
                                            <span>Пароль:</span>  <input type="text" name="to_password" value="<?php echo $password; ?>" placeholder="Введите ссылку"></br>
                                        </div>
                                        <div class="to_options_string">
                                            <span>Точка входа (POS):</span>  <input type="text" name="to_param_pos" value="<?php echo $param_pos; ?>" placeholder="Введите ссылку"></br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table class="form-table">
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Наценка на билеты в %:</td>
                                <td valign="top">
                                    <div class="to_options">
                                        <div class="price_title">
                                            <p>Порог до (руб)</br>(цена одного билета)</p>
                                            <p>Наценка в %</p>
                                        </div>
                                        <div class="to_options_string" id="cost_item">
                                            <?php
                                            if ($to_cost) {
                                                $t = 1;
                                                foreach ($to_cost as $key => $value) { ?>
                                                    <div class="opt_string_wrap" id="str<?php echo $t; ?>">
                                                        <input type="number" min="0" name="to_cost_max[]" value="<?php echo $key; ?>" placeholder="0">
                                                        <input type="number" min="0" max="100" name="to_cost[]" value="<?php echo $value; ?>" placeholder="0">
                                                        <?php if ($t > 1) { ?>
                                                            <button class="remove_row">-</button>
                                                        <?php } ?>
                                                    </div>
                                                    <?php $t++;
                                                }
                                            } else { ?>
                                            <div class="opt_string_wrap" id="str1">
                                                <input type="number" min="0" name="to_cost_max[]" value="" placeholder="0">
                                                <input type="number" min="0" max="100" name="to_cost[]" value="" placeholder="0">
                                            </div>
                                         <?php } ?>
                                        </div>
                                        <button  id="add_rov" class="add_rov">Добавить поле</button>
                                        <script>
                                        jQuery(document).ready(function () {
                                            jQuery(document).on('click', '#add_rov', function (e) {
                                                e.preventDefault();
                                                var item = jQuery('#cost_item');
                                                var rowNum = parseInt(item.find('div.opt_string_wrap:last').attr('id').slice(3)) + 1;
                                                item.append(
                                                    '<div class="opt_string_wrap" id="str' + rowNum + '"><input type="number" min="0" name="to_cost_max[]" value="" placeholder="0">' +
                                                    '<input type="number" min="0"  max="100" name="to_cost[]" value="" placeholder="0"><button class="remove_row">-</button></div>'
                                                );
                                            });
                                            jQuery(document).on('click', '.remove_row', function (e) {
                                                jQuery(this).parent().remove();
                                            });
                                        });
                                        </script>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Ссылка на страницу формы для выбора билетов:</td>
                                <td valign="top">
                                    <div class="to_options">
                                        <div class="to_options_string to_options_string2">
                                            <input type="text" name="to_link_to_ticket" value="<?php echo $to_link_to_ticket; ?>" placeholder="<?php echo $site_url; ?>...."></br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Ссылка на страницу для отображения маршрутов:</td>
                                <td valign="top">
                                    <div class="to_options">
                                        <div class="to_options_string to_options_string2">
                                            <input type="text" name="to_link_to_route_page" value="<?php echo $to_link_to_route_page; ?>" placeholder="<?php echo $site_url; ?>...."></br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Ссылка на страницу оформления заказа:</td>
                                <td valign="top">
                                    <div class="to_options">
                                        <div class="to_options_string to_options_string2">
                                            <input type="text" name="to_link_to_order" value="<?php echo $to_link_to_order; ?>" placeholder="<?php echo $site_url; ?>...."></br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Ссылка на страницу c базовым расписанием поездов:</td>
                                <td valign="top">
                                    <div class="to_options to_options_string2">
                                        <div class="to_options_string">
                                            <input type="text" name="to_link_to_shedule_page" value="<?php echo $to_link_to_shedule_page; ?>" placeholder="<?php echo $site_url; ?>...."></br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" width="25%" style="font-weight: 600;">Ссылка на страницу c правилами обработки персональных данных:</td>
                                <td valign="top">
                                    <div class="to_options to_options_string2">
                                        <div class="to_options_string">
                                            <input type="text" name="to_link_to_rule_page" value="<?php echo $to_link_to_rule_page; ?>" placeholder="<?php echo $site_url; ?>...."></br>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <p class="submit">
                            <input type="submit" name="Submit" class="button-primary" value="Сохранить настройки" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="submit" name="Reload" class="button-primary" value="Обновить данные о городах, странах и станциях" />
                        </p>
                        <div>*После установки плагина, или при изменении настроек доступа к API РЖД, обязательно обновите данные о городах, странах и станциях!</div>
                    </div>
                </form>
                <h3> Для выводы формы поиска и оформления билетов на сайте используйте шоткод &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[to_form] </h3>
                <h3> Для вывода маршрутов поездов используйте шоткод &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[to_route] </h3>
                <h3> Для вывода формы оформления заказа используйте шоткод &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[to_order] </h3>
                <h3> Для вывода базовго расписания поездов используйте шоткод &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[to_shedule] </h3>
                <h3> Для вывода формы личного кабинета покупателя используйте шоткод &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[to_customer] </h3>
                <div class="to_cron_url"><p style="font-size: 15px;">Cсылка для добавления в планировщик (автоматизация обновления данных о городах, странах и станциях):</p>
                    <p style="font-size: 15px;color: #2c05f5;"><?php echo $site_url; ?>/wp-content/plugins/ticket-online/ticket_online_update.php?to=<?php echo get_option('to_secret'); ?></p>
                </div>
            </div>
            <div class="gen_tab_item " data-link="seo">
                <div class="tab_content">
                    <div class="seo_tickets_container" id="seo1">
                        <div class="line_number"><span>1</span></div>
                        <div class="seo_tickets_box_list">
                            <div class="direction_to direction">
                                <div class="seo_input_box">
                                    <input class="name0_start" id="name0" type="text" name="st0" value="" autocomplete="off" placeholder="Откуда">
                                    <input id="name0_0" class="direction_value" type="hidden" name="st0_0" value="">
                                    <ul class="name0_start-search search_list"></ul>
                                </div>
                                <div class="seo_input_box">
                                    <input  class="name1_end" id="name1" type="text" name="st1" value="" autocomplete="off" placeholder="Куда">
                                    <input id="name1_1" class="direction_value_rev" type="hidden" name="st1_1" value="">
                                    <ul class="name1_end-search search_list"></ul>
                                </div>
                                <div class="seo_input_box">
                                    <div class="popup_condition" data-number="popup1">
                                        <input type="hidden" name="condition-field" class="condition-field from_field">
                                        <span class="if_field">Заполнено</span>
                                        <span class="if_empty">СЕО текст</span>
                                    </div>
                                </div>
                                <div class="seo_input_box">
                                    <div class="popup_condition" data-number="popup1_1">
                                        <input type="hidden" name="condition-field" class="condition-field rev_field">
                                        <span class="if_field">Заполнено</span>
                                        <span class="if_empty">СЕО текст</span>
                                    </div>
                                </div>
                                <div class="seo_input_box">
                                    <button class="btn_save">Сохранить</button>
                                </div>
                            </div>
                        </div>
                        <div class="remove_list_item">
                            <!-- <div class="delete_item">-</div> -->
                            <div class="add_item">+</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="SEO_popup" id="">
        <div class="form_wrapp">
            <div class="bg_dark"></div>
            <form action="">
                <div class="btns_pop_box">
                    <div class="close_popup">Закрыть</div>
                </div>
                <textarea name="" id="" class="seo_textarea" placeholder="Введите текст для СЕО"></textarea>
            </form>
        </div>
    </div>
    <script>
    jQuery(document).ready(function () {
        // Табы на странице Основные настройки / CEO настройки
        jQuery('.generel_link').click(function () {
            var gtId = jQuery(this).attr('id');
            if (jQuery(this).hasClass('active') !== true) {
                jQuery('.generel_link').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.gen_tab_item').removeClass('open');
                jQuery('.gen_tab_item[data-link=' + gtId + ']').addClass('open');
            }
        });
        // Проверка заполнения CEO текста
        function PopupTextareaCheck() {
            jQuery('.popup_condition').each(function () {
                var popText = jQuery(this).find('.condition-field').val();
                if (popText === false) {
                    jQuery(this).find('.if_empty').css('display', 'block');
                    jQuery(this).find('.if_field').css('display', 'none');
                } else {
                    jQuery(this).find('.if_field').css('display', 'block');
                    jQuery(this).find('.if_empty').css('display', 'none');
                }
            })
        }
        PopupTextareaCheck();
        // Вызов pop-up окна
        jQuery(document).on('click', '.popup_condition', function () {
            var thisDataId = jQuery(this).data('number');
            var thisCondition = jQuery(this).find('.condition-field').val();
            jQuery('.SEO_popup').addClass('open').attr('id', '' + thisDataId + '').stop(false, true).fadeIn(300);
            jQuery('.SEO_popup').find('form').attr('id', 'form_' + thisDataId + '');

            if (thisCondition === '') {
                // Если поле пустое открываем пустой редактор
                jQuery('.SEO_popup').find('textarea').val('');
            } else {
                // Если поле заполнено открываем его содержимое
                jQuery('.SEO_popup').find('textarea').val(thisCondition);
            }
        });
        // Закрываем pop-up
        jQuery('.close_popup, .bg_dark').live('click', function () {
            var popupText = jQuery('.SEO_popup').find('.seo_textarea').val(),
                popupID = jQuery(document).find('.SEO_popup').attr('id'),
                // ищем нужную строку
                findCondition = jQuery(document).find('.popup_condition[data-number=' + popupID + ']');
            // Записываем содержимое textarea в скрытый input этой строки
            findCondition.find('.condition-field').val(popupText);
            PopupTextareaCheck();
            jQuery('.SEO_popup').removeClass('open').stop(false, true).fadeOut(300);
        });
        // Добавляем новые поля для ввода
        jQuery('.add_item').live('click', function () {
            var contentForm = jQuery(this).parent().parent()
            newContent = parseInt(contentForm.attr('id').slice(3)) + 1;
            jQuery('.tab_content').append('<div class="seo_tickets_container" id="seo' + newContent + '"><div class="line_number"><span>' + newContent + '</span></div><div class="seo_tickets_box_list"><div class="direction_to direction"><div class="seo_input_box"><input class="name0_start" id="name' + newContent + '" type="text" name="st' + newContent + '" value="" autocomplete="off" placeholder="Откуда"><input id="name' + newContent + '_0" class="direction_value" type="hidden" name="st' + newContent + '_0" value=""><ul class="name0_start-search search_list"></ul></div><div class="seo_input_box"><input  class="name1_end" id="name' + newContent + '_1" type="text" name="st' + newContent + '_1" value="" autocomplete="off" placeholder="Куда"><input   id="name' + newContent + '_2" type="hidden" class="direction_value_rev" name="st' + newContent + '_2" value=""><ul class="name1_end-search search_list"></ul></div><div class="seo_input_box"><div class="popup_condition" data-number="popup' + newContent + '"><input type="hidden" name="condition-field" class="condition-field from_field"><span class="if_field">Заполнено</span><span class="if_empty">СЕО текст</span></div></div><div class="seo_input_box"><div class="popup_condition" data-number="popup' + newContent + '_1"><input type="hidden" name="condition-field" class="condition-field rev_field"><span class="if_field">Заполнено</span><span class="if_empty">СЕО текст</span></div></div><div class="seo_input_box"><button class="btn_save">Сохранить</button></div></div></div><div class="remove_list_item"><div class="delete_item">-</div><div class="add_item fadeIn">+</div></div></div>');
            PopupTextareaCheck();
            jQuery(this).removeClass('fadeIn').addClass('fadeOut').fadeOut();
        });
        // Удаляем поля для ввода
        jQuery(document).on('click', '.delete_item', function () {
        // Если это последний элемент списка
            if (jQuery(this).next().hasClass('fadeIn')) {
                jQuery(this).parent().parent().prev().find('.add_item').removeClass('fadeOut').addClass('fadeIn').fadeIn();
            }
            jQuery(this).parent().parent().remove();
            // Если остаётся один элемент
            if (jQuery('.seo_tickets_container').length < 2) {
                jQuery('.seo_tickets_container').find('.add_item').fadeIn();
            }
        });
        jQuery(document).on('click', '.btn_save', function () {
            var directionFrom = jQuery(this).parent().parent().find('.direction_value').val(),
                directionTo = jQuery(this).parent().parent().find('.direction_value_rev').val(),
                condFieldFrom = jQuery(this).parent().parent().find('.from_field').val();
            condFieldTo = jQuery(this).parent().parent().find('.rev_field').val();
            console.log('Откуда: ' + directionFrom + ', Куда: ' + directionTo + ', СЕО откуда: ' + condFieldFrom + ', СЕО Куда: ' + condFieldTo + '');
        })
    })
    </script>
</div>

<?php

// Получение баланса партнёра

function to_balance($authdata, $host_api, $param_pos) {
    $api_method = 'Partner/V1/Info/Balances';
    $data_string = '{}';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $host_api . "/" . $api_method);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 320);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Authorization: Basic ' . $authdata,
        'Accept: application/json',
        'Content-Type: application/json',
        'cache-control: no-cache',
        'Content-Encoding: gzip',
        'pos: ' . $param_pos
    ]);

    if (curl_exec($ch) === false) {
        echo 'Curl error: ' . curl_error($ch);
    }

    $errors = curl_error($ch);
    $result = curl_exec($ch);
    $returnCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    $out = json_decode($result, true);

    if ($out) {
        $html = '<ul>';
        foreach ($out["AccountBalances"] as $data) {
            $AccountName = $data["AccountName"];
            $CurrentBalance = $data["CurrentBalance"];
            $html .= '<li>' . $AccountName . ' - ' . $CurrentBalance . '</li>';
        }
        $html .= '</ul>';
    }

    return $html;
}

?>

<script>
jQuery(document).ready(function ($) {
// input станции отправления
    jQuery(document).on('keydown', '.name0_start', function (eventObject) {
        var searchTerm = jQuery(this).val(),
            activeElement = jQuery(this);
        var data = {
            action: 'get_city',
            'term': searchTerm
        };
        jQuery.post(ajaxurl, data, function (response) {
            activeElement.parent().find('.name0_start-search').fadeIn().html(response);
        });
    });
    jQuery(document).on('click', function (event) {
        if (jQuery(event.target).closest(".name0_start-search").length)
            return;
        jQuery(".name0_start-search").fadeOut();
        event.stopPropagation();
    });
    jQuery(document).on('click', '.name0_start-search li', function (event) {
        var node_id = jQuery(this).children().data("node_id");
        var node_nameru = jQuery(this).children().data("node_nameru");
        jQuery(this).parent().parent().find(".name0_start").val(node_nameru);
        jQuery(this).parent().parent().find(".direction_value").val(node_id);
        jQuery(".name0_start-search").fadeOut();
        event.stopPropagation();
    });
// input станции прибытия
    jQuery(document).on('keydown', '.name1_end', function (eventObject) {
        var searchTerm = jQuery(this).val();
        activeElement1 = jQuery(this);
        var data = {
            action: 'get_city',
            'term': searchTerm
        };
        jQuery.post(ajaxurl, data, function (response) {
            activeElement1.parent().find('.name1_end-search').fadeIn().html(response);
        });
    });
    jQuery(document).click(function (event) {
        if (jQuery(event.target).closest(".name1_end-search").length)
            return;
        jQuery(".name1_end-search").fadeOut();
        event.stopPropagation();
    });
    jQuery(document).on('click', '.name1_end-search li', function (event) {
        var node_id = jQuery(this).children().data("node_id");
        var node_nameru = jQuery(this).children().data("node_nameru");
        jQuery(this).parent().parent().find(".name1_end").val(node_nameru);
        jQuery(this).parent().parent().find(".direction_value_rev").val(node_id);
        jQuery(".name1_end-search").fadeOut();
        event.stopPropagation();
    });
});
</script>
